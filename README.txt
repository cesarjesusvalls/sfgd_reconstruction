*************************************************************************
*                                                                       *
* SFGD Reconstruction software                                          *
* César Jesús-Valls. // cjesus@ifae.es.                                 *
*                                                                       *
* ***********************************************************************


/////////********** /////////**********/////////********** /////////

To install the package:

$ cd /utils
$ rootcint -f  mydict.C -c LinkDef.h

To run one example:

$ cd /macros
$ source make.sh ana_example.C
$ ./ana_example.C

 ... it can also be run with: (in general much slower)

 root -l ana_skeleton.C

/////////********** /////////**********/////////********** /////////


********************
General information:
********************

This software is organised in the following way:

/src/       :  Contains the core files of the package including the reconstruction classes and MC generator.

/macros/    :  Personal macros must be placed here.

/utils/     :  Contains auxiliar files as a dictionary and the global header.

/data/      :  Can be used to store MC events or real data to perform either analysis or debbuging. 

/app/       :  Macros can be compiled to /app/ folder to be used independently to send jobs.


*******************************
How to write and run the code:
*******************************

-> At the beginning of your macro add:

    THIS_NAME = name of the void mainfunction() of the macro

* Special options for the macros can be achived by also writing:

    INTERACTIVE_OUTPUT to go to root prompt at the end of the execution
    OVERRIDE_OPTONS to manage yourself command line arguments  (eg -b for batch processing is therefore disabled)

-> Write your macro and store it in /macros/ with the extension '.C'.
-> Compile it using the command line: $source make.sh yourMacro.C
   automatically a yourMacro.exe file will be generated. Run it normally.
-> Additionaly, the macro can be compile directly to /app/ by typing:
   $root -l yourMacro.C


****************
For developers:
****************

!!  In the case you are going to /src/ files, please, follow the next coding convention:  !!

Variables:

   A short, but meaningful, name that communicates to the casual observer what the variable represents, rather than how it is used.
   Begin with a lowercase letter and use camel case (mixed case, starting with lower case).   

   Examples: mass // auxiliarVector // depositedEnergy

Constant:

    Use all capital letters, separating internal words with the underscore character.

    Examples: BOLTZMANN // MAX_HEIGHT

Class: 

    A noun that communicates what the class represents. Begin with an uppercase letter and use camel case for internal words.

    Examples: Event // Voxel // ProtoTrack

Method:
    A verb that communicates what the method does. Begin with a uppercase letter and use camel case for internal words.  

    Examples: Draw // DrawAll


Input for functions starts with 'p_', while internal class variables start with 'f', in order to clearly distinguish them from other
variables used in the implementation of the function.


***************
TStyle options
***************

t2kStyle can be found in /utils/globalTools.C
To use it, add the following lines to your macro:

...................................................................
  TString localStyleName = "T2K";
  // -- WhichStyle --
  // 1 = presentation large fonts
  // 2 = presentation small fonts
  // 3 = publication/paper
  Int_t localWhichStyle = T2KstyleIndex-1;

  TStyle* t2kstyle = SetT2KStyle(localWhichStyle, localStyleName);
  gROOT->SetStyle(t2kstyle->GetName());
...................................................................
