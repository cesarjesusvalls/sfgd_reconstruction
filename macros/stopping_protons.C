#define THIS_NAME stopping_protons
#define NOINTERACTIVE_OUTPUT
#define OVERRIDE_OPTIONS

#include "../utils/global_header.h"

void stopping_protons() {

  vector<TString> listOfFiles;
  TString file    = "/Users/cjesus/Documents/PhD/SFGD/25August_8/25August_8_MCR0_hadrons_0pt8Gev_0pt0T_Beam___NewStructure.root";
  TString prefix  = "";
  for (int iarg=0; iarg<gApplication->Argc(); iarg++){
    if (string( gApplication->Argv(iarg))=="-f" || string( gApplication->Argv(iarg))=="--file" ){
      iarg++;
      file = gApplication->Argv(iarg);
      if (file.Contains(".root")) {
        if (CheckFile(file.Data()) == 1) { printf("Source must be a _NewStructure.root file \n"); exit(1); }
        cout << "adding filename" <<" " << file << endl;
        listOfFiles.push_back(file);
      } else {
        fstream fList(file);
        if (fList.good()) {
          while (fList.good()) {
            string filename;
            getline(fList, filename);
            if (fList.eof()) break;
            if (CheckFile(filename) == 1) { printf("Source must be a _NewStructure.root file \n"); exit(1); }
            cout << "adding filename" <<" " << filename << endl;
            listOfFiles.push_back(filename);
          }
        }
      }
    } else if (string( gApplication->Argv(iarg))=="-o" || string( gApplication->Argv(iarg))=="--output" ){
      iarg++;
      prefix = gApplication->Argv(iarg);
    } else if (string( gApplication->Argv(iarg))=="-h" || string( gApplication->Argv(iarg))=="--help" ){
      cout << "**************************************" << endl;
      cout << "Macros run options:" << endl;
      cout << "   -f || --file      input file (*.root) or filelist" << endl;
      cout << "   -o || --output    output folder for plots and out file" << endl;
      cout << "   -h || --help      print help info" << endl;
      cout << "**************************************" << endl;
    }
  }

  if(listOfFiles.size() == 0) {listOfFiles.push_back(file); cout << "adding default file" <<" " << file << endl;}

  //Use TChain to merge all files in a single file.
  TChain* data = new TChain("AllEvents");
  for (uint ifile=0;ifile< listOfFiles.size();ifile++){
    file = listOfFiles[ifile];
    data->AddFile(file);
  }

  // Specify directory for output Event Display PNG
  TString foutDir;
  if(prefix == "") foutDir = GetDir(listOfFiles[0].Data()) + "/StoppingProtonStudies/";
  else foutDir = prefix + "/StoppingProtonStudies/";
 
  string createFolder ="mkdir -p ";
  createFolder +=  foutDir.Data();
  system(createFolder.c_str());

  Int_t cnt = 0;
  Int_t maxPNGnum = 50;

  // ostringstream foutPNGnum;
  // string foutPNG;

  // Loading the Events Tree_______________________________________________________________________

  TClonesArray* Hits=0;
  Event *event = new Event();

  data->SetBranchAddress("Event", &event);

  Int_t nevent = 10000;
  // if (nevent <= 0) nevent = data->GetEntries();
  // if (nevent > data->GetEntries()) nevent = data->GetEntries();

  cout << nevent << endl;


  for (Int_t i=0;i<nevent;i++) {
     data->GetEntry(i);        //read event
     Hits = event->GetHits();       //get pointer to the TClonesArray object
     Hits->Clear();                 //clear it
  }


  // Defining output directory/tree____________________________________________________________________

  TTree *SelectedEvents = new TTree("SelectedEvents", "SelectedEvents");
  Event* SelEvent = new Event();
  SelectedEvents->Branch("Event", "Event", SelEvent);

  // Creating Histograms and Canvases_________________________________________________________________

  TH2F *event_XY = new TH2F("","", 24,0,24, 8,0,8);
  event_XY->SetTitle("XY View;X axis [cm];Y axis[cm];Energy [p.e.]");

  TH2F *event_ZY = new TH2F("","", 48,0,48, 8,0,8);
  event_ZY->SetTitle("ZY View;Z axis [cm];Y axis [cm];Energy [p.e.]");

  TH2F *event_XZ = new TH2F("","", 24,0,24, 48,0,48);
  event_XZ->SetTitle("XZ View;X axis [cm];Z axis [cm];Energy [p.e.]");

  TH1D *dEdzXZ = new TH1D("","",48,0,48);
  dEdzXZ->SetTitle("dE/dz in XZ;Z axis [cm];dE/dz [p.e.]");

  TH1D *dEdzZY = new TH1D("","",48,0,48);
  dEdzZY->SetTitle("dE/dz in ZY;Z axis [cm];dE/dz [p.e.]");

  TH1D *dEdzSUM = new TH1D("","",48,0,48);
  dEdzSUM->SetTitle("dE/dz in XZ and ZY;Z axis [cm];dE/dz [p.e.]");

  TH1D *dEdz_ZatMax = new TH1D("dEdz at ZatMax","dEdz at ZatMax",2500,0,2500);
  TH1D *dEdz_ZatMaxPlusOne = new TH1D("dEdz at ZatMax+1","dEdz at ZatMax+1",2000,0,2000);
  TH1D *dEdz_ZatMaxPlusTwo = new TH1D("dEdz at ZatMax+2","dEdz at ZatMax+2",2000,0,2000);

  TH1D *All_Events = new TH1D("All_Events", "All_Events", 100, 0 ,100);
  TH1D *All_Events_norm = new TH1D("All_Events_norm", "All_Events_norm", 100, 0 ,100);

  TH2F *KEvsRange = new TH2F("KEvsRange","KEvsRange",48,0,48, 14900, 950, 15050);

  TH2F *EnergyVsDt = new TH2F("","", 150,-150,0,250,0,250);
  EnergyVsDt->SetTitle("Energy vs. #Deltat;#Deltat [2.5 ns];Energy [p.e.]");


  array<Double_t, 24> dEdxXZ;
  array<Double_t, 8> dEdyZY;

  array<Int_t, 48> XZocc;
  array<Int_t, 48> ZYocc;

  Double_t totE;

  TCanvas *c1 = new TCanvas("c1","c1", 2200, 1200);
  c1->Divide(3,2);

  // Filling Histograms_______________________________________________________________________________

  //loop over events
  for (int iev=0; iev<nevent; iev++){
    cout<<"_Processing events..."<<(int)(iev*100/(Double_t)nevent)<<"% done\r"<<flush;
    data->GetEntry(iev);
    totE=0;

    //loop over hits
    for (int ihit=0; ihit<event->GetNHits(); ihit++){
      Hit *hit = (Hit*)Hits->At(ihit);

      EnergyVsDt->Fill(hit->GetDt(),hit->GetCharge(),1);
      
      //Apply appropriate cut on Dt based on particle type
      if (hit->GetDt()<-60 && hit->GetDt()>-80){
        Hit* SelHit = SelEvent->AddHit();
        SelHit->SetAll(hit);

        if (hit->GetView()==0) event_XY->Fill(hit->GetX(),hit->GetY(),hit->GetCharge());
        if (hit->GetView()==1) {
          totE+=hit->GetCharge();
          event_XZ->Fill(hit->GetX(),hit->GetZ(),hit->GetCharge());
          dEdzXZ->Fill(hit->GetZ(),hit->GetCharge());
          dEdzSUM->Fill(hit->GetZ(),hit->GetCharge());
          dEdxXZ[hit->GetX()]+=hit->GetCharge();
          if (hit->GetCharge()>0) XZocc[hit->GetZ()]+=1;
        }
        if (hit->GetView()==2) {
          totE+=hit->GetCharge();
          event_ZY->Fill(hit->GetZ(),hit->GetY(),hit->GetCharge());
          dEdzZY->Fill(hit->GetZ(),hit->GetCharge());
          dEdzSUM->Fill(hit->GetZ(),hit->GetCharge());
          dEdyZY[hit->GetY()]+=hit->GetCharge();
          if (hit->GetCharge()>0) ZYocc[hit->GetZ()]+=1;
        }
      }
    }

    // Applying Selection________________________________________________________________________________

    if (event_XZ->GetEntries()>10 && Selection(event, event_XY, dEdxXZ, dEdyZY)==0){
      Int_t ZatMax = dEdzSUM->GetMaximumBin();

      dEdz_ZatMax->Fill(dEdzSUM->GetBinContent(ZatMax));
      dEdz_ZatMaxPlusOne->Fill(dEdzSUM->GetBinContent(ZatMax+1));
      dEdz_ZatMaxPlusTwo->Fill(dEdzSUM->GetBinContent(ZatMax+2));

      for (int Z = 0; Z < 48; Z++ ){
          SelEvent->SetOccupancyXZ(Z,XZocc[Z]);
          SelEvent->SetOccupancyZY(Z,ZYocc[Z]);
          SelEvent->SetdEdzXZ(Z,dEdzXZ->GetBinContent(Z));
          SelEvent->SetdEdzZY(Z,dEdzZY->GetBinContent(Z));
          SelEvent->SetdEdz(Z,dEdzSUM->GetBinContent(Z));
      }

      SelEvent->SetFEB12ch(event->GetFEB12ch());
      SelEvent->SetFEB12LeadTime(event->GetFEB12LeadTime());
      SelEvent->SetRange(ZatMax);
      SelEvent->SetMaxCharge(dEdzSUM->GetBinContent(ZatMax));
      SelEvent->SetEventID(iev);

      SelectedEvents->Fill();

      KEvsRange->Fill(ZatMax, totE, 1);

      for (int i = 0; i<ZatMax; i++){
        Double_t x = dEdzSUM->GetBinContent(ZatMax-i);
        All_Events->Fill(50-i,x);
        if (x>0) {All_Events_norm->Fill(50-i,1);}
      }

      for (int i = 1; i<=47-ZatMax; i++){
        Double_t x = dEdzSUM->GetBinContent(ZatMax+i);
        All_Events->Fill(50+i,x);
        if (x>0) {All_Events_norm->Fill(50+i,1);}
      }

    if(maxPNGnum > cnt){
    c1->cd(1);
    event_XY->Draw("COLZ");
    c1->cd(2);
    event_XZ->GetZaxis()->SetRangeUser(0.1,600);
    event_XZ->Draw("COLZ");
    c1->cd(3);
    event_ZY->GetZaxis()->SetRangeUser(0.1,600);
    event_ZY->Draw("COLZ");
    c1->cd(4);
    dEdzXZ->Draw("COLZ");
    c1->cd(5);
    dEdzZY->Draw("COLZ");
    c1->cd(6);
    EnergyVsDt->Draw("COLZ");

    c1->Update();
    TString nameout = (foutDir+ "image_%d", cnt);
    c1->SaveAs(TString::Format(foutDir+ "name_%d.pdf", cnt));
    cnt++;
    }

    }
    // Reset all histograms
    event_XY->Reset();
    event_XZ->Reset();
    event_ZY->Reset();
    dEdzXZ->Reset();
    dEdzZY->Reset();
    dEdzSUM->Reset();
    EnergyVsDt->Reset();

    dEdxXZ.fill(0);
    dEdyZY.fill(0);
    XZocc.fill(0);
    ZYocc.fill(0);

    SelEvent->Clear();

  }
  cout<<"_Processing events...100% done"<<endl;


  // Write to .root file ________________________________________________________________________


  TString fileNameOut = foutDir + "stopping_protons.root";
  TFile *FileOutput = new TFile(fileNameOut.Data(),"RECREATE");

  FileOutput->cd();

  cout<<"Writing  "<<foutDir.Data() <<endl;

  SelectedEvents->Write();

  KEvsRange->Write();
  dEdz_ZatMax->Write();
  dEdz_ZatMaxPlusOne->Write();
  dEdz_ZatMaxPlusTwo->Write();

  TH1D *All_Events_normalized = (TH1D*)All_Events->Clone("All_Events_normalized");
  All_Events_normalized->Divide(All_Events_norm);

  All_Events->GetXaxis()->SetLimits(-50,50);
  All_Events_normalized->GetXaxis()->SetLimits(-50,50);

  All_Events_normalized->Write();

  FileOutput->Close();

  cout << "Program finished" << endl;

  return;
}
