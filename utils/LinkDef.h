#include "../src/Classes/Hit.hxx"
#include "../src/Classes/Event.hxx"

#include "../src/Classes/Voxel.hxx"
#include "../src/Classes/Track.hxx"
#include "../src/Classes/ProtoTrack.hxx"
#include "../src/Classes/RecoEvent.hxx"

#include <vector>

#ifdef __MAKECINT__

#pragma link C++ class vector<int>;
#pragma link C++ class vector<vector<int> > ;
#pragma link C++ class vector<short>;
#pragma link C++ class vector<vector<short> > ;
#pragma link C++ class vector<vector<double> >;

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;


// Unpacking Classes
#pragma link C++ class Event+;
#pragma link C++ class Hit+;

// Reconstruction Classes
#pragma link C++ class Voxel+;
#pragma link C++ class VoxelSet+;
#pragma link C++ class Global+;
#pragma link C++ class RecoEvent++;
#pragma link C++ class ProtoTrack+;
#pragma link C++ class Track+;

#endif // __CINT__
