#ifndef _VOXEL_H_
#define _VOXEL_H_

#include <TObject.h>
#include <iostream>

class Voxel: public TObject{
    
private:

    vector <Hit*>   fHits;              // Collection of pointers to hits used to reconstruct this voxel
    vector <Int_t>  fHitsIdx;           // Index associated to each fHits component within an Event loop over hits.
    Int_t           fX;                 // X position
    Int_t           fY;                 // Y position
    Int_t           fZ;                 // Z position 
    Double_t        fT;                 // Reconstructed Time
    Double_t        fQ;                 // Reconstructed Charge
    Int_t           fPrTrId;            // This voxel belongs to the ProtoTrack with ID fPrTrId
    Int_t           fTrId;              // This voxel belongs to the Track with ID fPrTrId
    Int_t           fVtxId;             // ... about to be changed.
    Int_t           fTrueId;              //0 == core , 1 == xTalk,  2 == ghost
    Int_t           fRecoId;              //0 == core , 1 == no-core
    Hit* auxHit1 = new Hit();
    Hit* auxHit2 = new Hit();
    Hit* auxHit3 = new Hit();

public:

    Voxel(){
        this->Reset();
    };

    Voxel(Double_t p_fX, Double_t p_fY, Double_t p_fZ){
        this->Reset();
        this->SetPos(p_fX,p_fY,p_fZ);
    };

    Voxel(Double_t p_fX, Double_t p_fY, Double_t p_fZ, Double_t p_fT, Double_t p_fQ){
        this->Reset();
        Voxel::SetPos(p_fX,p_fY,p_fZ);
        fT = p_fT;
        fQ = p_fQ;
    };

    virtual ~Voxel(){
        delete auxHit1;
        delete auxHit2;
        delete auxHit3;
    };

    //functions to store data
    void SetX(Int_t p_fX){ fX = p_fX; }
    void SetY(Int_t p_fY){ fY = p_fY; }
    void SetZ(Int_t p_fZ){ fZ = p_fZ; }
    void SetCharge(Double_t p_fQ){ fQ = p_fQ; }
    void SetTime(Double_t p_fT){ fT = p_fT; }
    void SetProtoTrackId(Int_t p_fPrTrId){ fPrTrId = p_fPrTrId; }
    void SetTrackId(Int_t p_fTrId){ fTrId = p_fTrId; }
    void SetVertexId(Int_t p_fVtxId){ fVtxId = p_fVtxId; }
    void SetTrueId(Int_t p_fTrueId){  fTrueId = p_fTrueId; } 
    void SetRecoId(Int_t p_fRecoId){  fRecoId = p_fRecoId; } 
    void SetHits(vector <Hit*> p_fHits){ fHits = p_fHits;} 
    void SetHit( Int_t p_N , Hit* p_fHit){ fHits[p_N] = p_fHit;} 
    void SetHitsIdx(vector <Int_t> p_fHitsIdx){ fHitsIdx = p_fHitsIdx;} 
    void SetPos(Double_t p_fX, Double_t p_fY, Double_t p_fZ)
         {
            this->SetX(p_fX);
            this->SetY(p_fY);
            this->SetZ(p_fZ);
         }

    //functions to retrieve data
    Int_t GetX(){ return fX; }
    Int_t GetY(){ return fY; }
    Int_t GetZ(){ return fZ; }
    Double_t GetTime(){ return fT; }
    Double_t GetCharge(){ return fQ; }
    Int_t GetTrackId(){ return fTrId; }
    Int_t GetTrueId(){ return fTrueId; }
    Int_t GetRecoId(){ return fRecoId; }
    Int_t GetProtoTrackId(){ return fPrTrId; }
    Int_t GetVertexId(){ return fVtxId; }
    vector <Hit*> GetHits(){ return fHits; }
    vector <Int_t> GetHitsIdx(){ return fHitsIdx; }

    //methods
    void Reset(Option_t* /*option*/="")
    {
        fHits.clear();
        fHits.reserve(3);
        fX      = -1;
        fY      = -1;
        fZ      = -1;
        fQ      = -1;
        fT      = -1;
        fPrTrId = -1;
        fTrId   = -1;
        fVtxId  = -1;
        fTrueId = -1;
        fRecoId = -1; 
        fHits.push_back(auxHit1);
        fHits.push_back(auxHit2);
        fHits.push_back(auxHit3);
    }
    ClassDef(Voxel,1);
};

#endif
