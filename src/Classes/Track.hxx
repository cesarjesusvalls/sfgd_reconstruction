#ifndef _TRACK_H_
#define _TRACK_H_

#include "VoxelSet.hxx"

#include <TNtuple.h>
#include <TStyle.h>
#include <TROOT.h>

class Track: public VoxelSet{

protected:

    TNtuple* fData;           // Auxiliar object for 3D Drawing.
    vector <Double_t> fDir;

public:

    //constructors
    Track(){};

    //destructor
    virtual ~Track(){};

    //functions to store data
    void SetData(TNtuple* p_fData){ fData = p_fData;}
    void SetDir(vector <Double_t> p_fDir) {fDir = p_fDir;}

    //functions to retrieve data
    TNtuple* GetData(){return fData;}
    vector <Double_t> GetDir(){return fDir;}

    void RecoDraw(string name, bool all);

    Track(vector <Voxel*> p_fVoxels){
        AddVoxels(p_fVoxels);
        // gStyle->SetCanvasColor(0);
        // gStyle->SetMarkerStyle(21);
        // gStyle->SetMarkerSize(1.05);
        fData= new TNtuple("fData", "fData", "x:y:z:color");
    }

    ClassDef(Track,1);
};

#endif
