#ifndef _PROTO_TRACK_H_
#define _PROTO_TRACK_H_

#include <TH2F.h>
#include <iostream>

class ProtoTrack: public Track{ 

private:
    vector <Voxel*> fVertex;
    Double_t fFitQuality[3];
    TH2F* f2DView[3];
public:

    //constructors
    ProtoTrack(){};

    ProtoTrack(vector <Voxel*> p_fVoxels){
        std::cout << "constructing ProtoTrack" << std::endl;
        AddVoxels(p_fVoxels);
        gStyle->SetCanvasColor(0);
        gStyle->SetMarkerStyle(21);
        gStyle->SetMarkerSize(1.4);
        fData = new TNtuple("fData", "fData", "x:y:z:color");
    }

    //destructor
    virtual ~ProtoTrack(){std::cout << "destroying ProtoTrack" << std::endl;};

    //functions to store data
    void SetVertex(vector <Voxel*> p_fVertex){
        for(uint i=0; i<p_fVertex.size(); i++){    
            fVertex.push_back(p_fVertex[i]);
        }
    }

    void Set2DView(Int_t p_N, TH2F* p_View) {f2DView[p_N] = p_View;}
    void SetFitQuality(Int_t p_N, Double_t p_fFitQuality) {fFitQuality[p_N] = p_fFitQuality;}

    //functions to retrieve data
    vector <Voxel*> GetVertex(){return fVertex;}
    TH2F* Get2DView(Int_t p_N) {return f2DView[p_N];}
    Double_t GetFitQuality(Int_t p_N) {return fFitQuality[p_N];}

    //methods
    void SearchVertex();

    void Reset(Option_t* /*option*/="")
    {
        fVoxels.clear();  
        fVertex.clear();
    }

    ClassDef(ProtoTrack,1);
};

#endif
