#include "ProtoTrack.hxx"

#include <TF1.h>

// //********************************************************************
// void ProtoTrack::SearchVertex(){
// //********************************************************************

//     vector <int> x = this->GetX();
//     vector <int> y = this->GetY();
//     vector <int> z = this->GetZ();

//     double MIN_DISTANCE = 1.01;
//     vector <double> distVector;
//     double distance;

//     for(uint i=0; i<fVoxels.size(); i++){
//         int counter = 0;
//         double totDistance=0;
//         for(uint j=0; j<fVoxels.size(); j++){
//             distance =  pow(pow(abs(x[i]-x[j]),2)+pow(abs(y[i]-y[j]),2)+pow(abs(z[i]-z[j]),2),0.5);
//             if(distance < MIN_DISTANCE){
//                 counter++;
//                 totDistance+=distance;
//             }
//         }
//         distVector.push_back(totDistance/counter);
//     }

//     double maxDist = 0;
//     int iMaxDist = -1;

//     for(uint i=0; i<fVoxels.size(); i++){
//     if(distVector[i]>maxDist) {maxDist = distVector[i]; iMaxDist = i;}
//     }

//     vector <Voxel*> vertex;
//     vector <Voxel*> noVertex;
//     for(uint j=0; j<fVoxels.size(); j++){
//         distance =  pow(pow(abs(x[iMaxDist]-x[j]),2)+pow(abs(y[iMaxDist]-y[j]),2)+pow(abs(z[iMaxDist]-z[j]),2),0.5);
//         Int_t xV = this->GetVoxel(j)->GetX();
//         Int_t yV = this->GetVoxel(j)->GetY();
//         Int_t zV = this->GetVoxel(j)->GetZ();
//         if(distance < MIN_DISTANCE){

//             f2DView[0]->SetBinContent(yV+1,xV+1,0.5);
//             f2DView[1]->SetBinContent(zV+1,xV+1,0.5);
//             f2DView[2]->SetBinContent(zV+1,yV+1,0.5);

//             vertex.push_back(this->GetVoxel(j));
//             fVoxels[j]->SetVertexId(1);
//         }
//         else {
//             f2DView[0]->SetBinContent(yV+1,xV+1,1);
//             f2DView[1]->SetBinContent(zV+1,xV+1,1);
//             f2DView[2]->SetBinContent(zV+1,yV+1,1);
//             // noVertex.push_back(this->GetVoxel(j));
//             // noVertex[noVertex.size()-1]->SetProtoTrackId(-1);
//         }
//     }

//     this->SetVertex(vertex);
// }


//********************************************************************
void ProtoTrack::SearchVertex(){
//********************************************************************

    vector <int> x = this->GetX();
    vector <int> y = this->GetY();
    vector <int> z = this->GetZ();

    vector <double> distVector;

    Int_t sView = 0;

    fFitQuality[1] > fFitQuality[2] ? sView = 1 : sView = 2;

    TF1* fitCut = NULL;

    if(sView == 1){
        cout << "The critical differentiation is at the XZ plane; FitQuality: " << fFitQuality[1] << endl;
        fitCut = f2DView[1]->GetFunction("pol1") ;
    }
    if(sView == 2){
        cout << "The critical differentiation is at the YZ plane; FitQuality: " << fFitQuality[2] << endl;
        fitCut = f2DView[2]->GetFunction("pol1"); 
        return;
    }

    TH2F *newXY = new TH2F("newXY", "newXY", 8, 0., 8., 24, 0., 24.);        // X and Y switch to rotate view
    TH2F *newXZ = new TH2F("newXZ", "newXZ", 48, 0., 48., 24, 0., 24.);      // X and Z switch to rotate view
    TH2F *newYZ = new TH2F("newYZ", "newYZ", 48, 0., 48., 8, 0., 8.);        // Y and Z switch to rotate view

    Int_t track1 = 0;
    Int_t track2 = 0;

    for(uint j=0; j<fVoxels.size(); j++){
        Int_t xV = this->GetVoxel(j)->GetX();
        Int_t yV = this->GetVoxel(j)->GetY();
        Int_t zV = this->GetVoxel(j)->GetZ();
        if(sView == 1){
            if(xV+0.5 > fitCut->Eval(zV) || ( abs(xV+0.5 - fitCut->Eval(zV) ) < 0.5 ) ){
                f2DView[0]->SetBinContent(yV+1,xV+1,0.5);
                f2DView[1]->SetBinContent(zV+1,xV+1,0.5);
                f2DView[2]->SetBinContent(zV+1,yV+1,0.5);

                track1++;
            }
            else {
                f2DView[0]->SetBinContent(yV+1,xV+1,1);
                f2DView[1]->SetBinContent(zV+1,xV+1,1);
                f2DView[2]->SetBinContent(zV+1,yV+1,1);

                track2++;
            }
        }
    }

    for(uint j=0; j<fVoxels.size(); j++){
        Int_t xV = this->GetVoxel(j)->GetX();
        Int_t yV = this->GetVoxel(j)->GetY();
        Int_t zV = this->GetVoxel(j)->GetZ();
        if(sView == 1){
            if(xV+0.5 > fitCut->Eval(zV) || ( abs(xV+0.5 - fitCut->Eval(zV) ) < 0.5 ) ){
                if(track1 >= track2){
                    newXY->SetBinContent(yV+1,xV+1,1);
                    newXZ->SetBinContent(zV+1,xV+1,1);
                    newYZ->SetBinContent(zV+1,yV+1,1);
                }
            }
            else {
                if(track1 < track2){
                    newXY->SetBinContent(yV+1,xV+1,1);
                    newXZ->SetBinContent(zV+1,xV+1,1);
                    newYZ->SetBinContent(zV+1,yV+1,1);
                }
            }
        }
    }


    if(sView == 1){
        TCanvas * newC = new TCanvas("newC");
        newXZ->Fit("pol1");
        fitCut = newXZ->GetFunction("pol1");
        cout << "newFitQuality: " << fitCut->GetChisquare() /  fitCut->GetNDF() << endl;
        newXZ->Draw("COLZ");
        newC->Update();
        newC->WaitPrimitive();
    }

     for(uint j=0; j<fVoxels.size(); j++){
        Int_t xV = this->GetVoxel(j)->GetX();
        Int_t yV = this->GetVoxel(j)->GetY();
        Int_t zV = this->GetVoxel(j)->GetZ();
        if(sView == 1){
            if(( abs(xV+0.5 - fitCut->Eval(zV) ) < 1.2 ) ){
                f2DView[0]->SetBinContent(yV+1,xV+1,0.5);
                f2DView[1]->SetBinContent(zV+1,xV+1,0.5);
                f2DView[2]->SetBinContent(zV+1,yV+1,0.5);
            }
            else {
                f2DView[0]->SetBinContent(yV+1,xV+1,1);
                f2DView[1]->SetBinContent(zV+1,xV+1,1);
                f2DView[2]->SetBinContent(zV+1,yV+1,1);
            }
        }
    }   

    //this->SetVertex(vertex);
}
