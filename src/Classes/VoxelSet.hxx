#ifndef _VOXEL_SET_H
#define _VOXEL_SET_H

#include <TObject.h>

class VoxelSet: public TObject{

protected:
    
    vector <Voxel*> fVoxels;          // Set of voxels

public:

    //constructors
    VoxelSet(){        
        Reset();
    };

    //destructor
    virtual ~VoxelSet(){};

    //functions to store data
    void AddVoxels(vector <Voxel*> p_fVoxels){ fVoxels = p_fVoxels;}

    //functions to retrieve data
    vector <int> GetX(){
        vector <int> vec;
        for(UInt_t i=0; i<fVoxels.size(); i++){
            vec.push_back(fVoxels[i]->GetX());
        }
        return vec;
    }
    vector <int> GetY(){
        vector <int> vec;
        for(UInt_t i=0; i<fVoxels.size(); i++){
            vec.push_back(fVoxels[i]->GetY());
        }
        return vec;
    }
    vector <int> GetZ(){
        vector <int> vec;
        for(UInt_t i=0; i<fVoxels.size(); i++){
            vec.push_back(fVoxels[i]->GetZ());
        }
        return vec;
    }
    vector <Double_t> GetCharge(){
        vector <Double_t> vec;
        for(UInt_t i=0; i<fVoxels.size(); i++){
            vec.push_back(fVoxels[i]->GetCharge());
        }
        return vec;
    }

    Voxel* GetVoxel(int p_N){return fVoxels[p_N];}
    vector <Voxel*> GetVoxels(){return fVoxels;}


    void Reset(Option_t* /*option*/="")
    {
        fVoxels.clear();  
    }


    ClassDef(VoxelSet,1);
};

#endif
