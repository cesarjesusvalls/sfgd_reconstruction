#include "Track.hxx"

#include <TCanvas.h>
#include <TH3.h>

//********************************************************************
void Track::RecoDraw(string name="same", bool all=kTRUE){
//********************************************************************
        vector <int> x = this->GetX();
        vector <int> y = this->GetY();
        vector <int> z = this->GetZ();

        if(name == "same") fData = (TNtuple*) gROOT->FindObject("fData"); 
        else if (name == "clean"){
            fData = (TNtuple*) gROOT->FindObject("fData");
            fData->Reset();
            return;
        }
        
        float lastColor = 0;

        if(all){
            TTree *t1 = (TTree*) fData->CloneTree();
            t1->SetBranchAddress("color", &lastColor);
            t1->GetEntry(t1->GetEntries()-1);
        }

        for(uint i=0; i<x.size(); i++){
            fData->Fill(x[i], y[i], z[i], lastColor+1);
        }

        if((TCanvas*) gROOT->FindObject("c1")){
            TCanvas *oldCanvas = (TCanvas*) gROOT->FindObject("c1"); 
            oldCanvas->Close();
            TCanvas *c1 = new TCanvas("c1", "c1", 800, 600, 800, 600); c1->cd();
        }
        else {TCanvas *c1 = new TCanvas("c1", "c1", 800, 600, 800, 600); c1->cd();}

        fData->Draw("x:y:z:color","","box");
        TH3F *htemp = (TH3F*)gPad->GetPrimitive("htemp");
        htemp->GetZaxis()->SetLimits(-12,36);
        htemp->GetYaxis()->SetLimits(-20,28);
        htemp->GetXaxis()->SetLimits(0,48);
        htemp->SetTitle("");

        gPad->Update();
}
