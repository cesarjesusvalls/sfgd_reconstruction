#include "RecoEvent.hxx"
#include <TF1.h>
#include "../Voxel3Dfit.C"
#include <TGraph2D.h>
#include <TPolyLine3D.h>

//***********************************************************************************************
void RecoEvent::TagxTalkFlag(){
//***********************************************************************************************

    Hit* AddHit(TClonesArray* HitsArray, int HitNum);

    array<TClonesArray*, 48> HitsPerLayer_XZ;
    array<TClonesArray*, 48> HitsPerLayer_ZY;

    for (int j=0; j<48;j++){
        HitsPerLayer_XZ[j]=new TClonesArray("Hit", 50);
        HitsPerLayer_ZY[j]=new TClonesArray("Hit", 50);
    }

    array<Int_t, 48> NHitsPerLayer_XZ;
    array<Int_t, 48> NHitsPerLayer_ZY;

    for (int j=0; j<48;j++){
      HitsPerLayer_XZ[j]->Clear("C");
      HitsPerLayer_ZY[j]->Clear("C");
      NHitsPerLayer_XZ[j]=0;
      NHitsPerLayer_ZY[j]=0;
    }

    Event* SelEvent = new Event();

    for (int ihit=0; ihit<fEvent->GetNHits(); ihit++){
      Hit *hit = (Hit*) fHits->At(ihit);

      //Apply appropriate cut on Dt based on particle type
    // if(fMC) hit->SetDt(-70);
    //   if (hit->GetDt()<-60 && hit->GetDt()>-80){
        if(kTRUE){
            if (hit->GetView()==0) {
              Hit* XYHit = SelEvent->AddHit();
              XYHit->SetAll(hit);
              XYHit->SetxTalkFlag(0);
            }
            if (hit->GetView()==1) {
              Hit* hit_in_layer=AddHit(HitsPerLayer_XZ[hit->GetZ()], NHitsPerLayer_XZ[hit->GetZ()]);
              NHitsPerLayer_XZ[hit->GetZ()]++;
              hit_in_layer->SetAll(hit);
            }

            if (hit->GetView()==2) {
              Hit* hit_in_layer=AddHit(HitsPerLayer_ZY[hit->GetZ()], NHitsPerLayer_ZY[hit->GetZ()]);
              NHitsPerLayer_ZY[hit->GetZ()]++;
              hit_in_layer->SetAll(hit);
            }
        }
    }

    // xTalk Flag________________________________________________________________________________

    int hitnum_maxE =0;
    vector<Double_t> hitsE_inLayer;

    for (int f=0; f<48; f++){ //loop over layers

      hitsE_inLayer.clear();

      //------------------------------------------------------------------------
      //Checking the XZ view
      //------------------------------------------------------------------------
      if (NHitsPerLayer_XZ[f]>1){

        hitnum_maxE=0;
        Double_t MaxQ_inLayer=0;
        array<Int_t, 24> Xcells;
        Xcells.fill(0);


        for (int l=0; l<NHitsPerLayer_XZ[f]; l++){ //loop over hits in layer f
          Hit *hit2 = (Hit*)HitsPerLayer_XZ[f]->At(l);
          hitsE_inLayer.push_back(hit2->GetCharge());
        }

        MaxQ_inLayer = *max_element(hitsE_inLayer.begin(), hitsE_inLayer.end());
        transform(hitsE_inLayer.begin(), hitsE_inLayer.end(), hitsE_inLayer.begin(), bind2nd(divides<Double_t>(), MaxQ_inLayer));

        //Main Hit Check
        for (int l=0; l<NHitsPerLayer_XZ[f];l++){
          Hit *MainHit = (Hit*)HitsPerLayer_XZ[f]->At(l);
          if ((hitsE_inLayer.at(l)>0.1 && MaxQ_inLayer>20) || MainHit->GetCharge()==-1) {
            Xcells[MainHit->GetX()]=1;
          }
        }

        //xTalk Check
        for (int l=0; l<NHitsPerLayer_XZ[f];l++){

          Hit *xTalkHit = (Hit*)HitsPerLayer_XZ[f]->At(l);
          Hit* FlaggedHit = SelEvent->AddHit();
          FlaggedHit->SetAll(xTalkHit);
          FlaggedHit->SetxTalkFlag(0);


          if (Xcells[xTalkHit->GetX()]==0) {
            if (Xcells[xTalkHit->GetX()+1] == 1 || Xcells[xTalkHit->GetX()-1] == 1){
              //cout<<"xTalk Hit at Z="<<xTalkHit->GetZ()<<", X="<<xTalkHit->GetX()<<endl;
              FlaggedHit->SetxTalkFlag(1);
            }
            else{
              //cout<<"noise Hit at Z="<<xTalkHit->GetZ()<<", X="<<xTalkHit->GetX()<<endl;
              //implement noise flag?
            }
          }

          //check if a hit with charge=-1 is xtalk
          if (xTalkHit->GetCharge()==-1) {
            if (Xcells[xTalkHit->GetX()+1] == 1 || Xcells[xTalkHit->GetX()-1] == 1){
            //cout<<"xTalk Hit at Z="<<xTalkHit->GetZ()<<", X="<<xTalkHit->GetX()<<"charge = -1"<<endl;
            Xcells[xTalkHit->GetX()]=0;
            FlaggedHit->SetxTalkFlag(1);
            }
          }
        }

        //if there's one hit in the layer, it's automaticaly a main hit
      } else if (NHitsPerLayer_XZ[f]==1) {
        Hit* FlaggedHit = SelEvent->AddHit();
        Hit *mainHit = (Hit*)HitsPerLayer_XZ[f]->At(0);
        FlaggedHit->SetAll(mainHit);
        FlaggedHit->SetxTalkFlag(0);
      }

      //------------------------------------------------------------------------
      //Checking the ZY view
      //------------------------------------------------------------------------
      hitsE_inLayer.clear();
      if (NHitsPerLayer_ZY[f]>1){

        hitnum_maxE=0;
        Double_t MaxQ_inLayer=0;
        array<Int_t, 8> Ycells;
        Ycells.fill(0);


        for (int l=0; l<NHitsPerLayer_ZY[f]; l++){ //loop over hits in layer f
          Hit *hit2 = (Hit*)HitsPerLayer_ZY[f]->At(l);
          hitsE_inLayer.push_back(hit2->GetCharge());
        }

        MaxQ_inLayer = *max_element(hitsE_inLayer.begin(), hitsE_inLayer.end());
        transform(hitsE_inLayer.begin(), hitsE_inLayer.end(), hitsE_inLayer.begin(), bind2nd(divides<Double_t>(), MaxQ_inLayer));



        //Main Hit Check
        for (int l=0; l<NHitsPerLayer_ZY[f];l++){
          Hit *MainHit = (Hit*)HitsPerLayer_ZY[f]->At(l);
          if ((hitsE_inLayer.at(l)>0.1 && MaxQ_inLayer>20) || MainHit->GetCharge()==-1) {
            Ycells[MainHit->GetY()]=1;
          }
        }

        //xTalk Check
        for (int l=0; l<NHitsPerLayer_ZY[f];l++){

          Hit *xTalkHit = (Hit*)HitsPerLayer_ZY[f]->At(l);
          Hit* FlaggedHit = SelEvent->AddHit();
          FlaggedHit->SetAll(xTalkHit);
          FlaggedHit->SetxTalkFlag(0);


          if (Ycells[xTalkHit->GetY()]==0) {
            if (Ycells[xTalkHit->GetY()+1] == 1 || Ycells[xTalkHit->GetY()-1] == 1){
              //cout<<"xTalk Hit at Z="<<xTalkHit->GetZ()<<", Y="<<xTalkHit->GetY()<<endl;
              FlaggedHit->SetxTalkFlag(1);
            }
            else{
              //cout<<"noise Hit at Z="<<xTalkHit->GetZ()<<", Y="<<xTalkHit->GetY()<<endl;
              //implement noise flag?
            }
          }

          //check if a hit with charge=-1 is xtalk
          if (xTalkHit->GetCharge()==-1) {
            if (Ycells[xTalkHit->GetY()+1] == 1 || Ycells[xTalkHit->GetY()-1] == 1){
            //cout<<"xTalk Hit at Z="<<xTalkHit->GetZ()<<", Y="<<xTalkHit->GetY()<<"charge = -1"<<endl;
            Ycells[xTalkHit->GetY()]=0;
            FlaggedHit->SetxTalkFlag(1);
            }
          }
        }

        //if there's one hit in the layer, it's automaticaly a main hit
      } else if (NHitsPerLayer_ZY[f]==1) {
        Hit* FlaggedHit = SelEvent->AddHit();
        Hit* mainHit = (Hit*)HitsPerLayer_ZY[f]->At(0);
        FlaggedHit->SetAll(mainHit);
        FlaggedHit->SetxTalkFlag(0);
      }
    }

    //get the rest of the event information
    
    //SelEvent->SetEventID(iev);    // implement at the reco lvl!
    
    SelEvent->SetFEB12ch(fEvent->GetFEB12ch());
    SelEvent->SetFEB12LeadTime(fEvent->GetFEB12LeadTime());
    SelEvent->SetMaxCharge(fEvent->GetMaxCharge());
    SelEvent->SetRange(fEvent->GetRange());

    this->SetEvent(SelEvent);
}

//***********************************************************************************************
void RecoEvent::MergeHits(Double_t minQ=0){
//***********************************************************************************************
    
    Event * newEvent = new Event();

    cout << "Hits before Merging: " << fEvent->GetNHits() << endl;

    bool MergeV0 = kTRUE;
    bool MergeV1 = kTRUE;
    bool MergeV2 = kTRUE;

    Int_t tMin = fGlobal->GetMinHitTime();
    Int_t tMax = fGlobal->GetMaxHitTime();

    vector < Int_t > analyzedHits; 
    analyzedHits.reserve(fEvent->GetNHits());

    if(MergeV0){
        for (Int_t ihit=0; ihit< fEvent->GetNHits(); ihit++){
            bool doContinue = kFALSE;
            for (UInt_t k=0; k<analyzedHits.size(); k++){
                if(ihit == analyzedHits[k]) doContinue = kTRUE;
            }
            if(doContinue) continue;
            Hit* ahit = (Hit*) fHits->At(ihit);
            if(fMC) ahit->SetDt(-70);
            if(ahit->GetDt()> tMax || ahit->GetDt() < tMin) continue;
            if (ahit->GetCharge() <= 0) continue;
            if(ahit->GetView() != 0) continue;
            analyzedHits.push_back(ihit);
            double Charge0 = ahit->GetCharge();
            for (Int_t jhit=0; jhit< fEvent->GetNHits(); jhit++){
                if(ihit == jhit) continue;
                Hit* bhit = (Hit*) fHits->At(jhit);
                if (ahit->GetView() == 0 && bhit->GetView() == 0 && ahit->GetX() == bhit->GetX() && ahit->GetY() == bhit->GetY()){
                    Charge0+=bhit->GetCharge();
                    analyzedHits.push_back(jhit);
                }
            }
            Hit* newHit = newEvent->AddHit();
            newHit->SetView(0);
            newHit->SetX(ahit->GetX());
            newHit->SetY(ahit->GetY());
            newHit->SetZ(-1);
            newHit->SetCharge(Charge0);
            newHit->SetTrueXTalk(ahit->GetTrueXTalk());
        }
    }

    if(MergeV1){
        for (Int_t ihit=0; ihit< fEvent->GetNHits(); ihit++){
            bool doContinue = kFALSE;
            for (UInt_t k=0; k<analyzedHits.size(); k++){
                if(ihit == analyzedHits[k]) doContinue = kTRUE;
            }
            if(doContinue) continue;
            Hit* ahit = (Hit*) fHits->At(ihit);
            if(ahit->GetDt()> tMax || ahit->GetDt() < tMin) continue;
            if (ahit->GetCharge() <= 0) continue;
            if(ahit->GetView() != 1) continue;
            analyzedHits.push_back(ihit);
            double Charge1 = ahit->GetCharge();
            if(Charge1 < minQ) continue;
            for (Int_t jhit=0; jhit< fEvent->GetNHits(); jhit++){
                if(ihit == jhit) continue;
                Hit* bhit = (Hit*) fHits->At(jhit);
                if (ahit->GetView() == 1 && bhit->GetView() == 1 && ahit->GetX() == bhit->GetX() && ahit->GetZ() == bhit->GetZ()){
                    Charge1+=bhit->GetCharge();
                    analyzedHits.push_back(jhit);
                }
            }
            Hit* newHit = newEvent->AddHit();
            newHit->SetView(1);
            newHit->SetX(ahit->GetX());
            newHit->SetY(-1);
            newHit->SetZ(ahit->GetZ());
            newHit->SetCharge(Charge1);
            newHit->SetTrueXTalk(ahit->GetTrueXTalk());
        }
    }

    if(MergeV2){
        for (Int_t ihit=0; ihit< fEvent->GetNHits(); ihit++){
            bool doContinue = kFALSE;
            for (UInt_t k=0; k<analyzedHits.size(); k++){
                if(ihit == analyzedHits[k]) doContinue = kTRUE;
            }
            if(doContinue) continue;
            Hit* ahit = (Hit*) fHits->At(ihit);
            if(ahit->GetDt()> tMax || ahit->GetDt() < tMin) continue;
            if (ahit->GetCharge() <= 0) continue;
            if(ahit->GetView() != 2) continue;
            analyzedHits.push_back(ihit);
            double Charge2 = ahit->GetCharge();
            if(Charge2 < minQ) continue;
            for (Int_t jhit=0; jhit< fEvent->GetNHits(); jhit++){
                if(ihit == jhit) continue;
                Hit* bhit = (Hit*) fHits->At(jhit);
                if (ahit->GetView() == 2 && bhit->GetView() == 2 && ahit->GetY() == bhit->GetY() && ahit->GetZ() == bhit->GetZ()){
                    Charge2+=bhit->GetCharge();
                    analyzedHits.push_back(jhit);
                }
            }
            Hit* newHit = newEvent->AddHit();
            newHit->SetView(2);
            newHit->SetX(-1);
            newHit->SetY(ahit->GetY());
            newHit->SetZ(ahit->GetZ());
            newHit->SetCharge(Charge2);
            newHit->SetTrueXTalk(ahit->GetTrueXTalk());
        }
    }

    //Add hits without merging 
    for (Int_t ihit=0; ihit< fEvent->GetNHits(); ihit++){
        bool doContinue = kFALSE;
        for (UInt_t k=0; k<analyzedHits.size(); k++){
            if(ihit == analyzedHits[k]) doContinue = kTRUE;
        }
        if(doContinue) continue;
        Hit* ahit = (Hit*) fHits->At(ihit);
        if(ahit->GetDt()> tMax || ahit->GetDt() < tMin) continue;
        if(ahit->GetCharge() < minQ) continue;
        Hit* newHit = newEvent->AddHit();
        if(ahit->GetView() == 0){
            newHit->SetView(0);
            newHit->SetX(-1);
            newHit->SetY(ahit->GetY());
            newHit->SetZ(ahit->GetZ());
            newHit->SetCharge(ahit->GetCharge());
            newHit->SetTrueXTalk(ahit->GetTrueXTalk());
        }
        if(ahit->GetView() == 1){
            newHit->SetView(1);
            newHit->SetX(ahit->GetX());
            newHit->SetY(-1);
            newHit->SetZ(ahit->GetZ());
            newHit->SetCharge(ahit->GetCharge());
            newHit->SetTrueXTalk(ahit->GetTrueXTalk());
        }
        if(ahit->GetView() == 2){
            newHit->SetView(2);
            newHit->SetX(-1);
            newHit->SetY(ahit->GetY());
            newHit->SetZ(ahit->GetZ());
            newHit->SetCharge(ahit->GetCharge());
            newHit->SetTrueXTalk(ahit->GetTrueXTalk());
        }
    }
    this->SetEvent(newEvent);
    cout << "Hits after merging: " << fEvent->GetNHits() << endl;

}

//***********************************************************************************************
void RecoEvent::HitsToVoxels(){
//***********************************************************************************************

    //This method reconstruct 3D voxels from 2D projections for a given Event.
    //It associates to each Voxel its coordinates (x,y,z) and the list of hits
    //used to reconstruct it. From the Voxel there is access to the list of hits
    //with Voxel::GetHits() allowing to access the whole information of every event.

    TClonesArray* p_Hits = fEvent->GetHits();

    vector<vector <Int_t>>  viewXY;      // To store XY projection coordinates in a hit 
    vector<vector <Int_t>>  viewXZ;      // To store XZ projection coordinates in a hit 
    vector<vector <Int_t>>  viewYZ;      // To store YZ projection coordinates in a hit 
    
    vector<vector <Int_t>>  vX;          // To match projections in coordinate X
    vector<vector <Int_t>>  vY;          // To match projections in coordinate Y
    vector<vector <Int_t>>  vZ;          // To match projections in coordinate Z

    vector<vector <Int_t>>  vXY;          // To match vX with vY
    vector<vector <Int_t>>  vXZ;          // To match vX with vZ
    vector<vector <Int_t>>  vYZ;          // To match vY with vZ

    vector<vector <Int_t>>  vXYZ_all;    // Contains all 3D matches from 2 projections
                                            // with double counting!
    vector<vector <Int_t>>  vXYZ_2D;
    vector<vector <Int_t>>  vXYZ_3D;

    vector <Int_t> auxVector;            // Auxiliar vector to fill the vectors of vectors

    Hit* auxHit;  // to get Hits on the loop. 
    vector <Int_t> auxVectorHitIdxs; // Auxiliar vector to store hits' indices from the event to the voxel
                                     // This is not clear to be a relevant variable and may be rm in the future
    vector<Voxel*> voxels; //To store the reconstructed voxels


    vX.clear();
    vY.clear();
    vZ.clear();

    vXY.clear();
    vXZ.clear();
    vYZ.clear();

    vXYZ_all.clear();
    vXYZ_3D.clear();

    auxVector.clear();
    auxVectorHitIdxs.clear();
    voxels.clear();

    for (Int_t ihit=0; ihit< fEvent->GetNHits(); ihit++){
        Hit* hit = (Hit*) p_Hits->At(ihit);

        if( hit->GetxTalkFlag() ){
            hit->SetCharge(1000); 
            continue;
        } 

        hit->SetMultiplicity(0);

        auxHit = new Hit();
        auxHit->SetAll(hit);

        // In principle the voxelisation could be coded more compactly by using
        // only 1 list of hit pointers and comparing its elements among themselves.

        auxVector.clear();
        auxVector.reserve(5);
        if(hit->GetView() == 0) {
            auxVector.push_back(hit->GetX());
            auxVector.push_back(hit->GetY());
            auxVector.push_back(ihit);
            auxVector.push_back(hit->GetCharge());
            viewXY.push_back(auxVector);
        }
        if(hit->GetView() == 1) {
            auxVector.push_back(hit->GetX());
            auxVector.push_back(hit->GetZ());
            auxVector.push_back(ihit);
            auxVector.push_back(hit->GetCharge());
            viewXZ.push_back(auxVector);
        }
        if(hit->GetView() == 2) {
            auxVector.push_back(hit->GetY());
            auxVector.push_back(hit->GetZ());
            auxVector.push_back(ihit);
            auxVector.push_back(hit->GetCharge());
            viewYZ.push_back(auxVector);
        }
    }

    for(UInt_t i=0; i<viewXY.size(); i++){
        for(UInt_t j=0; j<viewXZ.size(); j++){
            auxVector.clear();
            auxVector.reserve(5);
            if(viewXY[i][0] == viewXZ[j][0]){
                auxVector.push_back(viewXY[i][0]); //X
                auxVector.push_back(viewXY[i][1]); //Y 
                auxVector.push_back(viewXZ[j][1]); //Z
                auxVector.push_back(viewXY[i][2]); //ihit XY
                auxVector.push_back(viewXZ[j][2]); //ihit XZ
                vX.push_back(auxVector);
            }
        }   
    }

    for(UInt_t i=0; i<viewYZ.size(); i++){
        for(UInt_t j=0; j<viewXY.size(); j++){
            auxVector.clear();
            auxVector.reserve(5);
            if(viewYZ[i][0] == viewXY[j][1]){
                auxVector.push_back(viewXY[j][0]); //X
                auxVector.push_back(viewXY[j][1]); //Y
                auxVector.push_back(viewYZ[i][1]); //Z
                auxVector.push_back(viewXY[j][2]); //ihit XY
                auxVector.push_back(viewYZ[i][2]); //ihit YZ
                vY.push_back(auxVector);
            }
        }   
    }

    for(UInt_t i=0; i<viewXZ.size(); i++){
        for(UInt_t j=0; j<viewYZ.size(); j++){
            auxVector.clear();
            auxVector.reserve(5);
            if(viewXZ[i][1] == viewYZ[j][1]){
                auxVector.push_back(viewXZ[i][0]); //X
                auxVector.push_back(viewYZ[j][0]); //Y
                auxVector.push_back(viewYZ[j][1]); //Z
                auxVector.push_back(viewXZ[i][2]); //ihit XZ
                auxVector.push_back(viewYZ[j][2]); //ihit YZ
                vZ.push_back(auxVector);
            }
        }   
    }

    // Imposse at least 2 matches from the 3 projections
    for(UInt_t i=0; i<vX.size(); i++){
        for(UInt_t j=0; j<vZ.size(); j++){
            auxVector.clear();
            auxVector.reserve(5);
            if(vX[i][0] == vZ[j][0] && vX[i][1] == vZ[j][1] && vX[i][2] == vZ[j][2]){
                auxVector.push_back(vX[i][0]);
                auxVector.push_back(vX[i][1]);
                auxVector.push_back(vX[i][2]);
                auxVector.push_back(vX[i][3]); //ihit XY
                auxVector.push_back(vX[i][4]); //ihit XZ
                auxVector.push_back(vZ[j][3]); //ihit XZ
                auxVector.push_back(vZ[j][4]); //ihit YZ
                vXZ.push_back(auxVector);     
            }
        }   
    }

    for(UInt_t i=0; i<vZ.size(); i++){
        for(UInt_t j=0; j<vY.size(); j++){
            auxVector.clear();
            auxVector.reserve(5);
            if(vZ[i][0] == vY[j][0] && vZ[i][1] == vY[j][1] && vZ[i][2] == vY[j][2]){
                auxVector.push_back(vZ[i][0]);
                auxVector.push_back(vZ[i][1]);
                auxVector.push_back(vZ[i][2]);
                auxVector.push_back(vZ[i][3]); //XZ
                auxVector.push_back(vZ[i][4]); //YZ
                auxVector.push_back(vY[j][3]); //XY
                auxVector.push_back(vY[j][4]); //YZ
                vYZ.push_back(auxVector);
            }
        }
    }

    for(UInt_t i=0; i<vY.size(); i++){
        for(UInt_t j=0; j<vX.size(); j++){
            auxVector.clear();
            auxVector.reserve(5);
            if(vY[i][0] == vX[j][0] && vY[i][1] == vX[j][1] && vY[i][2] == vX[j][2]){
                auxVector.push_back(vY[i][0]);
                auxVector.push_back(vY[i][1]);
                auxVector.push_back(vY[i][2]);
                auxVector.push_back(vY[i][3]); //XY
                auxVector.push_back(vY[i][4]); //YZ
                auxVector.push_back(vX[j][3]); //XY
                auxVector.push_back(vX[j][4]); //XZ         
                vXY.push_back(auxVector);
            }
        }
    }

    // Add all 3D matches from 2 projections to a common list 

    for(UInt_t i=0; i<vXY.size(); i++){
        auxVector.clear();
        auxVector.reserve(6);
        auxVector.push_back(vXY[i][0]);
        auxVector.push_back(vXY[i][1]);
        auxVector.push_back(vXY[i][2]);
        auxVector.push_back(vXY[i][3]); //XY
        auxVector.push_back(vXY[i][6]); //XZ
        auxVector.push_back(vXY[i][4]); //YZ
        vXYZ_all.push_back(auxVector);
    }
    for(UInt_t i=0; i<vXZ.size(); i++){
        auxVector.clear();
        auxVector.reserve(6);
        auxVector.push_back(vXZ[i][0]);
        auxVector.push_back(vXZ[i][1]);
        auxVector.push_back(vXZ[i][2]);
        auxVector.push_back(vXZ[i][3]); //XY
        auxVector.push_back(vXZ[i][4]); //XZ
        auxVector.push_back(vXZ[i][6]); //YZ
        vXYZ_all.push_back(auxVector);
    }
    for(UInt_t i=0; i<vYZ.size(); i++){
        auxVector.clear();
        auxVector.reserve(6);
        auxVector.push_back(vYZ[i][0]);
        auxVector.push_back(vYZ[i][1]);
        auxVector.push_back(vYZ[i][2]);
        auxVector.push_back(vYZ[i][5]); //XY
        auxVector.push_back(vYZ[i][3]); //XZ
        auxVector.push_back(vYZ[i][4]); //YZ
        vXYZ_all.push_back(auxVector);
    }

    //To clean up the double counted elements in vXYZ_all:
    for(UInt_t i=0; i<vXYZ_all.size(); i++){
        for(UInt_t j=0; j<vXYZ_all.size(); j++){
            if(j==i) continue;
            if (vXYZ_all[i][0] == vXYZ_all[j][0] && vXYZ_all[i][1] == vXYZ_all[j][1] &&
                vXYZ_all[i][2] == vXYZ_all[j][2] && vXYZ_all[i][3] == vXYZ_all[j][3] &&
                vXYZ_all[i][4] == vXYZ_all[j][4]){
            // mark repeated elements:
                vXYZ_all[j][0] = -1;
                vXYZ_all[j][1] = -1;
                vXYZ_all[j][2] = -1;
            }
        }   
    }

    for(UInt_t i=0; i<vXYZ_all.size(); i++){
        if(vXYZ_all[i][0] == -1)continue;   //ignore marked elements.
        auxVector.clear();
        auxVector.reserve(6);
        auxVector.push_back(vXYZ_all[i][0]); //X
        auxVector.push_back(vXYZ_all[i][1]); //Y
        auxVector.push_back(vXYZ_all[i][2]); //Z
        auxVector.push_back(vXYZ_all[i][3]); //hit1
        auxVector.push_back(vXYZ_all[i][4]); //hit2
        auxVector.push_back(vXYZ_all[i][5]); //hit3
        vXYZ_3D.push_back(auxVector);
    }

    for(UInt_t i=0; i<vXYZ_3D.size(); i++){
        auxVectorHitIdxs.clear();
        auxVectorHitIdxs.reserve(3);
        auxVectorHitIdxs[0] = -1;
        auxVectorHitIdxs[1] = -1;
        auxVectorHitIdxs[2] = -1;
        auxHit = (Hit*) p_Hits->At(vXYZ_3D[i][3]);
        auxVectorHitIdxs[auxHit->GetView()] = vXYZ_3D[i][3];
        auxHit = (Hit*) p_Hits->At(vXYZ_3D[i][4]);
        auxVectorHitIdxs[auxHit->GetView()] = vXYZ_3D[i][4];
        auxHit = (Hit*) p_Hits->At(vXYZ_3D[i][5]);
        auxVectorHitIdxs[auxHit->GetView()] = vXYZ_3D[i][5];
        vXYZ_3D[i][3] = auxVectorHitIdxs[0];
        vXYZ_3D[i][4] = auxVectorHitIdxs[1];
        vXYZ_3D[i][5] = auxVectorHitIdxs[2];
    }

    //maybe now is not necessary!
    //To clean up double counted elements (created while merging 2D in 3D):
    for(UInt_t i=0; i<vXYZ_3D.size(); i++){
        for(UInt_t j=0; j<vXYZ_3D.size(); j++){
            if(j==i) continue;
            if (vXYZ_3D[i][3] == vXYZ_3D[j][3] && vXYZ_3D[i][4] == vXYZ_3D[j][4]
                && vXYZ_3D[i][5] == vXYZ_3D[j][5]){
            //mark repeated elements
                vXYZ_3D[j][3] = -1;
                vXYZ_3D[j][4] = -1;
                vXYZ_3D[j][5] = -1;
            }
        }
    }

    for(UInt_t i=0; i<vXYZ_3D.size(); i++){
        if(vXYZ_3D[i][3] == -1 || vXYZ_3D[i][4] == -1 || vXYZ_3D[i][5] == -1) continue; //ignore repeated elements

        //store hit indices
        auxVectorHitIdxs.clear();

        auxVectorHitIdxs.push_back(vXYZ_3D[i][3]);
        auxVectorHitIdxs.push_back(vXYZ_3D[i][4]);
        auxVectorHitIdxs.push_back(vXYZ_3D[i][5]);     

        //store hit pointers   
        Voxel* auxVox = new Voxel(vXYZ_3D[i][0],vXYZ_3D[i][1],vXYZ_3D[i][2]);

        auxVox->SetHit(0, (Hit*) p_Hits->At(vXYZ_3D[i][3]));
        auxVox->SetHit(1, (Hit*) p_Hits->At(vXYZ_3D[i][4]));
        auxVox->SetHit(2, (Hit*) p_Hits->At(vXYZ_3D[i][5]));

        auxVox->GetHits()[0]->SetMultiplicity(auxVox->GetHits()[0]->GetMultiplicity()+1);
        auxVox->GetHits()[1]->SetMultiplicity(auxVox->GetHits()[1]->GetMultiplicity()+1);
        auxVox->GetHits()[2]->SetMultiplicity(auxVox->GetHits()[2]->GetMultiplicity()+1);

        auxVox->SetHitsIdx(auxVectorHitIdxs);
        voxels.push_back(auxVox);
    }

    this->AddVoxels(voxels);
    voxels.clear();

    //delete auxHit;
}

//*****************************************************************************
void RecoEvent::FindProtoTracks(vector <Int_t> x, vector <Int_t> y, vector <Int_t> z){
//*****************************************************************************

    const Double_t MIN_DISTANCE = 3;
    const Int_t MIN_VOXELS = 1;

    Int_t selected = 0;
    Int_t analysed = 0;
    Int_t close = 0;
    Int_t clusterNum = 0;
    Int_t counter = 0;
    Double_t distance = 0;
    Int_t voxelsInProtoTrack = 0;

    vector <Int_t> checked;
    vector <Int_t> toCheck;
    vector <Int_t> isolated;

    checked.resize(fVoxels.size());
    toCheck.resize(fVoxels.size());
    isolated.resize(fVoxels.size());

    for(uint i=0; i<fVoxels.size(); i++){
        if(!(fVoxels[i]->GetProtoTrackId() == -1)) continue;
        voxelsInProtoTrack = 0;
        toCheck[i] = 1;
        selected = 1;
        analysed = 0;

        while(analysed < selected){
            for(uint j=0; j<fVoxels.size(); j++){
                if(fVoxels[j]->GetProtoTrackId() != -1) continue;
                if(!toCheck[j]) continue;
                if(checked[j]) continue;
                if(isolated[j]) continue;
                analysed++;
                checked[j] = 1;
                close = 0;
                counter = 0;

                for(uint k=0; k<fVoxels.size(); k++){
                    if(k == j) continue; // avoid self check
                    distance =  pow(pow(abs(x[j]-x[k]),2)+pow(abs(y[j]-y[k]),2)+pow(abs(z[j]-z[k]),2),0.5);
                    if(distance <= MIN_DISTANCE){
                        if(!toCheck[k]) {toCheck[k] = 1; counter++;}
                        close++;
                    }
                }
                if(close >= MIN_VOXELS){
                    fVoxels[j]->SetProtoTrackId(clusterNum);
                    voxelsInProtoTrack++;
                    selected+=counter;
                }
                else{isolated[j]=1;}
            }
        }
        if(selected>1) {clusterNum++;}
        for(uint m=0; m<fVoxels.size(); m++){checked[m] = 0;}
    }

    for(uint m=0; m<fVoxels.size(); m++){
        if(isolated[m] && fVoxels[m]->GetProtoTrackId() == -1){
            fVoxels[m]->SetProtoTrackId(clusterNum);
            clusterNum++; 
        }
    }

    vector <Voxel*> tempVoxels;

    Int_t cN = 0;
    while(cN<clusterNum){
        tempVoxels.clear();
        for(int i=0; i< (int) fVoxels.size(); i++){
            if(fVoxels[i]->GetProtoTrackId() == cN) {tempVoxels.push_back(this->GetVoxel(i));}
        }
        if(tempVoxels.size()){            
            ProtoTrack* auxPrTrack = new ProtoTrack(tempVoxels);
            fPrTracks.push_back(auxPrTrack);
        }
        cN++;
    }
}

//*****************************************************************************
void RecoEvent::FillOcupancy(){
//*****************************************************************************
    if(!fVoxels.size()) return;

    for(Int_t i=0; i<this->GetEvent()->GetNHits(); i++){
        Hit* hit = (Hit*) fHits->At(i);
        if(hit->GetView() == 0) fGlobal->GetDist(0)->Fill(hit->GetX(),hit->GetY(),1);//hit->GetCharge());
        if(hit->GetView() == 1) fGlobal->GetDist(1)->Fill(hit->GetX(),hit->GetZ(),1);//hit->GetCharge());
        if(hit->GetView() == 2) fGlobal->GetDist(2)->Fill(hit->GetY(),hit->GetZ(),1);//hit->GetCharge());
    }
}


//*****************************************************************************
void RecoEvent::DrawEvent(bool p_DrawPrTracks = kFALSE){
//*****************************************************************************
    if(!fVoxels.size()) return;
    cout << "Drawing event with " << fVoxels.size() << " voxels." << endl;
    
    gStyle->SetOptStat(0);

    TH2F *hXY = new TH2F("hXY", "viewXY", 8, 0., 8., 24, 0., 24.);      // X and Y switch to rotate view
    TH2F *hXZ = new TH2F("hXZ", "viewXZ", 48, 0., 48., 24, 0., 24.);      // X and Z switch to rotate view
    TH2F *hYZ = new TH2F("hYZ", "viewYZ", 48, 0., 48., 8, 0., 8.);        // Y and Z switch to rotate view

    TH3F *hAll = new TH3F("hAll", "3D Event", 24, 0., 24., 8, 0, 8., 48, 0, 48.);

    Double_t Qmax1 = 0;
    Double_t Qmax2 = 0;
    Double_t Qmax3 = 0;

    for(Int_t i=0; i<this->GetEvent()->GetNHits(); i++){
        Hit* hit = (Hit*) fHits->At(i);
        if(hit->GetView() == 0){
            hXY->Fill(hit->GetY(),hit->GetX(),hit->GetCharge());
            if(hit->GetCharge()>Qmax1) Qmax1 = hit->GetCharge();
        }
        if(hit->GetView() == 1){
            hXZ->Fill(hit->GetZ(),hit->GetX(),hit->GetCharge());
            if(hit->GetCharge()>Qmax2) Qmax2 = hit->GetCharge();
        }
        if(hit->GetView() == 2){
            hYZ->Fill(hit->GetZ(),hit->GetY(),hit->GetCharge());
            if(hit->GetCharge()>Qmax3) Qmax3 = hit->GetCharge();
        }
    }

    for(UInt_t i=0; i<fVoxels.size(); i++) hAll->Fill(fVoxels[i]->GetX(),fVoxels[i]->GetY(),fVoxels[i]->GetZ());

    hXY->GetZaxis()->SetRangeUser(0.1,1.05*Qmax1);
    hXZ->GetZaxis()->SetRangeUser(0.1,1.05*Qmax2);
    hYZ->GetZaxis()->SetRangeUser(0.1,1.05*Qmax3);

    TCanvas *cc = new TCanvas("cc","cc",0, 0, 1000,1000);
    cc->Divide(2,2);

    cc->cd(1);
    hXY->Draw("COLZ");
    cc->cd(2);
    hXZ->Draw("COLZ");
    cc->cd(3);
    hYZ->Draw("COLZ");

    if   (p_DrawPrTracks) RecoEvent::DrawProtoTracks(0);
    else                   RecoEvent::ChargeDraw();


    cc->cd(4);
    hAll->Draw("BOX2");

    cc->Update();
    cc->WaitPrimitive();

    delete cc;

    if(gROOT->FindObject("fData")){
        TNtuple* auxNT = (TNtuple*) gROOT->FindObject("fData");
        delete auxNT;
    }
    delete hXY;
    delete hXZ;
    delete hYZ;
    delete hAll;
}

//*****************************************************************************
void RecoEvent::DrawProtoTracks(Int_t p_minVoxels=0){
//****************************************************************************
    cout << "drawing all selected prototracks with more than " << p_minVoxels << " voxels: [" << fPrTracks.size() << "]"  << endl;
    for(UInt_t i=0; i<fPrTracks.size(); i++){
        if(!i) fPrTracks[i]->RecoDraw("clean");
        if(fPrTracks[i]->GetVoxels().size() >= (uint) p_minVoxels) fPrTracks[i]->RecoDraw("same");
    }
}

//********************************************************************
void RecoEvent::ChargeDraw(){
//********************************************************************
        vector <int> x = this->GetX();
        vector <int> y = this->GetY();
        vector <int> z = this->GetZ();
        vector <Double_t> q = this->GetCharge();

        gStyle->SetCanvasColor(0);
        gStyle->SetMarkerStyle(21);
        gStyle->SetMarkerSize(1.05);
        TNtuple* fDataQ= new TNtuple("fDataQ", "fDataQ", "x:y:z:charge");

        for(uint i=0; i<x.size(); i++){
            fDataQ->Fill(x[i], y[i], z[i], q[i]);
        }

        if((TCanvas*) gROOT->FindObject("cQ")){
            TCanvas *oldCanvas = (TCanvas*) gROOT->FindObject("cQ"); 
            oldCanvas->Close();
            TCanvas *cQ = new TCanvas("cQ", "cQ", 800, 600, 800, 600); cQ->cd();
        }
        else {TCanvas *cQ = new TCanvas("cQ", "cQ", 800, 600, 800, 600); cQ->cd();}


        fDataQ->Draw("x:y:z:charge","","box");
        TH3F *htemp = (TH3F*)gPad->GetPrimitive("htemp");
        htemp->GetZaxis()->SetLimits(-12,36);
        htemp->GetYaxis()->SetLimits(-20,28);
        htemp->GetXaxis()->SetLimits(0,48);
        htemp->SetTitle("");

        gPad->Update();
}

//***********************************************************************************************
void RecoEvent::FindTrueVoxels(Double_t minQ=0){
//***********************************************************************************************

    if(!fMC){ cout << endl << "FindTrueVoxels can ONLY be used for MC!" << endl << endl; exit(1);}

    fTrueHits = fEvent->GetHits();

    //Each hit from MC contains 3 entries that can be accessed with ->GetTrueX() (also for Y and Z)
    //For each different X,Y,Z combination the trueQ of ->GetTrueChargeXY() (also for XZ and YZ) is summed.
    //To avoid doble counting the loop is only done for GetView == 0;
    //The output is the list of true voxels.
    //This has to be further tunned to allow ME selection (delayed hits)..., most likely usign PDG and trackID info.

    vector <Voxel*> trueVoxels;
    vector <Int_t> analyzedHits; 

    analyzedHits.reserve(fEvent->GetNHits());

    for (Int_t ihit=0; ihit< fEvent->GetNHits(); ihit++){
        bool doContinue = kFALSE;
        for (UInt_t k=0; k<analyzedHits.size(); k++){
            if(ihit == analyzedHits[k]) doContinue = kTRUE;
        }
        if(doContinue) continue;
        analyzedHits.push_back(ihit);
        Hit* hitA = (Hit*) fTrueHits->At(ihit);
        if(hitA->GetView() != 0) continue;
        vector <Hit*> selHits;
        selHits.push_back(hitA);
        Double_t TrueQa = ( hitA->GetTrueChargeXY()+hitA->GetTrueChargeXZ()+hitA->GetTrueChargeYZ() )/3;
        if(TrueQa < minQ) continue;
        for (Int_t jhit=0; jhit< fEvent->GetNHits(); jhit++){
            bool doContinue = kFALSE;
            for (UInt_t k=0; k<analyzedHits.size(); k++){
                if(jhit == analyzedHits[k]) doContinue = kTRUE;
            }
            if(doContinue) continue;
            Hit* hitB = (Hit*) fTrueHits->At(jhit);
            if(hitB->GetView() != 0) continue;
            Double_t TrueQb = ( hitB->GetTrueChargeXY()+hitB->GetTrueChargeXZ()+hitB->GetTrueChargeYZ() )/3;
            if(TrueQb < minQ) continue;
            if( hitA->GetTrueX() == hitB->GetTrueX() && hitA->GetTrueY() == hitB->GetTrueY() && hitA->GetTrueZ() == hitB->GetTrueZ()){
                selHits.push_back(hitB);
                analyzedHits.push_back(jhit);
                TrueQa+=TrueQb;
            }
        }
        Voxel* trueVox = new Voxel(selHits[0]->GetTrueX(),selHits[0]->GetTrueY(),selHits[0]->GetTrueZ());
        trueVox->SetCharge(TrueQa);
        trueVox->SetHits(selHits);
        if(hitA->GetTrueXTalk()) trueVox->SetTrueId(1);
        else if(!hitA->GetTrueXTalk()) trueVox->SetTrueId(0);
        else trueVox->SetTrueId(-1);
        trueVoxels.push_back(trueVox);
    }
    fTrueVoxels = trueVoxels;
    RecoEvent::FillTrueQHist();
}

//***********************************************************************************************
void RecoEvent::RemoveXTalk(){
//***********************************************************************************************

    if(!fMC){ cout << endl << "RemoveXTalk can ONLY be used for MC!" << endl << endl; exit(1);}

    Event * newEvent = new Event();

    for(Int_t ihit=0; ihit<fEvent->GetNHits(); ihit++){
        Hit* hit = (Hit*) fHits->At(ihit);
        if(hit->GetTrueXTalk()) continue;
        Hit* newHit = newEvent->AddHit();
        newHit->SetAll(hit);
        newHit->SetTrueXTalk(kFALSE);
        newHit->SetTrueX(hit->GetTrueX());
        newHit->SetTrueY(hit->GetTrueY());
        newHit->SetTrueZ(hit->GetTrueZ());
        newHit->SetTrueChargeXY(hit->GetTrueChargeXY());
        newHit->SetTrueChargeXZ(hit->GetTrueChargeXZ());
        newHit->SetTrueChargeYZ(hit->GetTrueChargeYZ());
    }
    this->SetEvent(newEvent);
}


//***********************************************************************************************
void RecoEvent::Reconstruct3DCharge(Int_t p_MaxMultiplicity = 3, bool fillGlobal=kTRUE){
//***********************************************************************************************

    for(UInt_t i=0; i<fVoxels.size(); i++){
        fVoxels[i]->SetCharge(-1);
    }

    for (Int_t m=1; m<=p_MaxMultiplicity; m++){
        for(UInt_t i=0; i<fVoxels.size(); i++){
            if(fVoxels[i]->GetCharge()>0) continue;
            Double_t recoCharge = 0;
            Int_t mHit0 = fVoxels[i]->GetHits()[0]->GetMultiplicity();
            Int_t mHit1 = fVoxels[i]->GetHits()[1]->GetMultiplicity();
            Int_t mHit2 = fVoxels[i]->GetHits()[2]->GetMultiplicity();
            if( mHit0 == m || mHit1 == m || mHit2 == m) {
                Double_t qHit0 = fVoxels[i]->GetHits()[0]->GetCharge();
                Double_t qHit1 = fVoxels[i]->GetHits()[1]->GetCharge();
                Double_t qHit2 = fVoxels[i]->GetHits()[2]->GetCharge();
                if (mHit0 == m && mHit1 == m && mHit2 == m){        
                    recoCharge = (qHit0+qHit1+qHit2) / 3;
                    if(fillGlobal) fGlobal->GetRecoQ(1+7*(m-1))->Fill(recoCharge); 
                }
                else if (mHit0 == m && mHit1 == m){
                    recoCharge = (qHit0+qHit1) / 2;
                    if(fillGlobal) fGlobal->GetRecoQ(2+7*(m-1))->Fill(recoCharge);
                }
                else if (mHit0 == m && mHit2 == m){
                    recoCharge = (qHit0+qHit2) / 2;
                    if(fillGlobal) fGlobal->GetRecoQ(3+7*(m-1))->Fill(recoCharge); 
                }
                else if (mHit1 == m && mHit2 == m){       
                    recoCharge = (qHit1+qHit2) / 2;
                    if(fillGlobal) fGlobal->GetRecoQ(4+7*(m-1))->Fill(recoCharge); 
                }
                else{
                    if(mHit0 == m){
                        recoCharge = qHit0;
                        if(fillGlobal) fGlobal->GetRecoQ(5+7*(m-1))->Fill(recoCharge);
                    }
                    else if(mHit1 == m){
                        recoCharge = qHit1;
                        if(fillGlobal) fGlobal->GetRecoQ(6+7*(m-1))->Fill(recoCharge);
                    }
                    else if(mHit2 == m){
                        recoCharge = qHit2;
                        if(fillGlobal) fGlobal->GetRecoQ(7+7*(m-1))->Fill(recoCharge);
                    }
                }
                fVoxels[i]->SetCharge(recoCharge);
                if(fillGlobal) fGlobal->GetRecoQ(0)->Fill(recoCharge);
            }
        }
    }
    for(UInt_t i=0; i<fVoxels.size(); i++){
        if(fVoxels[i]->GetCharge()>0) continue;
        fVoxels[i]->SetCharge(10000);
    }
}

//***********************************************************************************************
void RecoEvent::FillTrueQHist(){
//***********************************************************************************************

    if(!fMC){ cout << endl << "FillTrueQHist can ONLY be used for MC!" << endl << endl; exit(1);}

    for(UInt_t i=0; i<fTrueVoxels.size(); i++){
        fGlobal->GetTrueQ()->Fill(fTrueVoxels[i]->GetCharge());
    }

}


//***********************************************************************************************
void RecoEvent::FindGhosts(bool show3Dghosts = kFALSE){
//***********************************************************************************************

    if(!fMC){ cout << endl << "FindGhosts can ONLY be used for MC!" << endl << endl; exit(1);}

    for(UInt_t i=0; i<fVoxels.size(); i++){
    bool real = kFALSE;
        for(UInt_t j=0; j<fTrueVoxels.size(); j++){
            if(fVoxels[i]->GetX() == fTrueVoxels[j]->GetX() && fVoxels[i]->GetY() == fTrueVoxels[j]->GetY() && fVoxels[i]->GetZ() == fTrueVoxels[j]->GetZ() && !real){
                fGlobal->GetGhost(0)->Fill(fVoxels[i]->GetHits()[1]->GetCharge(),fVoxels[i]->GetHits()[2]->GetCharge());
                cout << fTrueVoxels[j]->GetTrueId() << endl;
                real = kTRUE;
            }   
        }
        if(!real){
            if(show3Dghosts) fVoxels[i]->SetCharge(50000);
            fNumGhosts++;
            fGlobal->GetGhost(1)->Fill(fVoxels[i]->GetHits()[1]->GetCharge(),fVoxels[i]->GetHits()[2]->GetCharge());
        }
    }
}


//***********************************************************************************************
void RecoEvent::FindXTalk(bool show3Dvoxels = kFALSE){
//***********************************************************************************************
    
    for(UInt_t i=0; i<fVoxels.size(); i++){
        for(UInt_t j=0; j<fTrueVoxels.size(); j++){
            if(fVoxels[i]->GetX() == fTrueVoxels[j]->GetX() && fVoxels[i]->GetY() == fTrueVoxels[j]->GetY() && fVoxels[i]->GetZ() == fTrueVoxels[j]->GetZ()){
                if(fVoxels[i]->GetTrueId() == 0 && fTrueVoxels[j]->GetTrueId() == 0) fGlobal->GetXTalkId()->Fill(0);
                if(fVoxels[i]->GetTrueId() == 1 && fTrueVoxels[j]->GetTrueId() == 1) fGlobal->GetXTalkId()->Fill(1);
                if(fVoxels[i]->GetTrueId() == 0 && fTrueVoxels[j]->GetTrueId() == 1) fGlobal->GetXTalkId()->Fill(2);
                if(fVoxels[i]->GetTrueId() == 1 && fTrueVoxels[j]->GetTrueId() == 0) fGlobal->GetXTalkId()->Fill(3);
                if(fTrueVoxels[j]->GetTrueId() == 1 && show3Dvoxels) fVoxels[i]->SetCharge(100000);
            }
        }
    }
}

//***********************************************************************************************
void RecoEvent::FindRecoXTalkMC(){
//***********************************************************************************************

    Double_t Good = 0;
    Double_t Bad  = 0;

    for(UInt_t i=0; i<fVoxels.size(); i++){
        for(UInt_t j=0; j<fTrueVoxels.size(); j++){
            if(fVoxels[i]->GetX() == fTrueVoxels[j]->GetX() && fVoxels[i]->GetY() == fTrueVoxels[j]->GetY() && fVoxels[i]->GetZ() == fTrueVoxels[j]->GetZ()){

                Double_t PullX = 0;
                Double_t PullY = 0;
                Double_t PullZ = 0;
                Double_t TotalPull = 0;
                Double_t AveQ = 0;

                Double_t m1  = fVoxels[i]->GetHits()[0]->GetCharge();
                Double_t m2  = fVoxels[i]->GetHits()[1]->GetCharge();
                Double_t m3  = fVoxels[i]->GetHits()[2]->GetCharge();

                Double_t Qxy  = fVoxels[i]->GetHits()[0]->GetCharge();
                Double_t Qxz  = fVoxels[i]->GetHits()[1]->GetCharge();
                Double_t Qyz  = fVoxels[i]->GetHits()[2]->GetCharge();

                Qxy > Qxz ? PullX = (Qxy - Qxz) / sqrt(Qxz) : PullX = (Qxy - Qxz) / sqrt(Qxy);
                Qxy > Qyz ? PullY = (Qxy - Qyz) / sqrt(Qyz) : PullY = (Qxy - Qyz) / sqrt(Qxy);
                Qxz > Qyz ? PullZ = (Qxz - Qyz) / sqrt(Qyz) : PullZ = (Qxz - Qyz) / sqrt(Qxz);
               
                if (m1 > m2 && m1 > m3) {TotalPull = PullZ; AveQ = (Qxz+Qyz)/2;}
                if (m2 > m1 && m2 > m3) {TotalPull = PullY; AveQ = (Qxy+Qyz)/2;}
                if (m3 > m1 && m3 > m2) {TotalPull = PullX; AveQ = (Qxy+Qxz)/2;}

                 Qxz > Qyz ? TotalPull = (Qxz - Qyz) / sqrt(Qxz) : TotalPull = (Qxz - Qyz) / sqrt(Qxz);;

                //AveQ = (Qxz+Qyz)/2;

                //if(Qxy < fGlobal->GetMinAveQcut() || Qxz < fGlobal->GetMinAveQcut() || Qyz < fGlobal->GetMinAveQcut()) fVoxels[i]->SetRecoId(1);

                if( abs(TotalPull) > fGlobal->GetPullZcut() || AveQ < fGlobal->GetMinAveQcut() ) fVoxels[i]->SetRecoId(1);
                else  fVoxels[i]->SetRecoId(0);

                fGlobal->GetPullZ(fTrueVoxels[j]->GetTrueId())->Fill(TotalPull);
                fGlobal->Get2DPullZ(fTrueVoxels[j]->GetTrueId())->Fill(TotalPull,(Qxz+Qyz)/2);

                if(fVoxels[i]->GetRecoId() != 0 && fTrueVoxels[j]->GetTrueId() != 0) {Good++; fGlobal->SetGood(fGlobal->GetGood()+1);}
                else if (fVoxels[i]->GetRecoId() == 0 && fTrueVoxels[j]->GetTrueId() == 0) {Good++; fGlobal->SetGood(fGlobal->GetGood()+1);}
                else {
                    // cout << "Qxz:" << Qxz << endl;
                    // cout << "Qyz:" << Qyz << endl;
                    // cout << "PullZ:" << PullZ << endl;

                    //fVoxels[i]->SetCharge(100000);
                    Bad++; 
                    fGlobal->SetBad(fGlobal->GetBad()+1);
                    if(fTrueVoxels[j]->GetTrueId() == 0){
                        fGlobal->SetLostCore(fGlobal->GetLostCore()+1);
                    }
                }
            }
        }
    }
    // cout << "GOOD: " << Good << endl;
    // cout << "BAD: "  << Bad << endl;
    // cout << "RATIO: " << 100*Good/(Good+Bad) << " %" << endl << endl;
}

//***********************************************************************************************
void RecoEvent::FindRecoXTalk(){
//***********************************************************************************************

    for(UInt_t i=0; i<fVoxels.size(); i++){
    
                Double_t Pull = 0;
                Double_t Qxz  = fVoxels[i]->GetHits()[1]->GetCharge();
                Double_t Qyz  = fVoxels[i]->GetHits()[2]->GetCharge();

                Qxz > Qyz ? Pull = (Qxz - Qyz) / sqrt(Qxz) : Pull = (Qxz - Qyz) / sqrt(Qxz);

                if( abs(Pull) > fGlobal->GetPullZcut() || (Qxz+Qyz)/2 < fGlobal->GetMinAveQcut() ) fVoxels[i]->SetRecoId(1);
                else  fVoxels[i]->SetRecoId(0);
    }
}

//***********************************************************************************************
void RecoEvent::RemoveRecoXTalk(){
//***********************************************************************************************

    vector <Voxel*> auxVox;

    for(UInt_t i=0; i<fVoxels.size(); i++){
        if(fVoxels[i]->GetRecoId()) continue;
        auxVox.push_back(fVoxels[i]);
    }

    fVoxels = auxVox;

}


//*****************************************************************************
void RecoEvent::FindTrackCandidates(Int_t maxIdx, bool p_Draw = kFALSE){
//*****************************************************************************

    for (UInt_t pT = 0; pT<fPrTracks.size(); pT++){

        if( (Int_t) pT != maxIdx) continue;


        vector <Voxel*> voxelsList = fPrTracks[pT]->GetVoxels();

        gStyle->SetOptStat(0);

        TH2F *hXY = new TH2F("hXY", "viewXY", 8, 0., 8., 24, 0., 24.);        // X and Y switch to rotate view
        TH2F *hXZ = new TH2F("hXZ", "viewXZ", 48, 0., 48., 24, 0., 24.);      // X and Z switch to rotate view
        TH2F *hYZ = new TH2F("hYZ", "viewYZ", 48, 0., 48., 8, 0., 8.);        // Y and Z switch to rotate view

        TH3F *hAll = new TH3F("hAll", "3D Event", 24, 0., 24., 8, 0, 8., 48, 0, 48.);

        Double_t Qmax1 = 0;
        Double_t Qmax2 = 0;
        Double_t Qmax3 = 0;

        for(UInt_t i=0; i<voxelsList.size(); i++){
            Int_t X = voxelsList[i]->GetX();
            Int_t Y = voxelsList[i]->GetY();
            Int_t Z = voxelsList[i]->GetZ();
            // Int_t Qxy = voxelsList[i]->GetHits()[0]->GetCharge();
            // Int_t Qxz = voxelsList[i]->GetHits()[1]->GetCharge();
            // Int_t Qyz = voxelsList[i]->GetHits()[2]->GetCharge();
            Int_t Qxy = 1;
            Int_t Qxz = 1;
            Int_t Qyz = 1;
            if(!hXY->GetBinContent(Y+1,X+1)){      // X and Y switch to rotate view
                hXY->Fill(Y,X,Qxy);
                if(Qxy>Qmax1) Qmax1 = Qxy;
            }
            if(!hXZ->GetBinContent(Z+1,X+1)){      // X and Z switch to rotate view
                hXZ->Fill(Z,X,Qxz);
                if(Qxz>Qmax2) Qmax2 = Qxz;
            }
            if(!hYZ->GetBinContent(Z+1,Y+1)){      // Y and Z switch to rotate view
                hYZ->Fill(Z,Y,Qyz);
                if(Qyz>Qmax3) Qmax3 = Qyz;
            }
        }

        hXY->Fit("pol1","Q");
        hXZ->Fit("pol1","Q");
        hYZ->Fit("pol1","Q");

        TF1* fitXY = hXY->GetFunction("pol1");
        TF1* fitXZ = hXZ->GetFunction("pol1");
        TF1* fitYZ = hYZ->GetFunction("pol1");

        Double_t X2xy  = fitXY->GetChisquare();
        Double_t NDFxy = fitXY->GetNDF();

        Double_t X2xz  = fitXZ->GetChisquare();
        Double_t NDFxz = fitXZ->GetNDF();

        Double_t X2yz  = fitYZ->GetChisquare();
        Double_t NDFyz = fitYZ->GetNDF();

        for(UInt_t i=0; i<voxelsList.size(); i++) hAll->Fill(voxelsList[i]->GetX(),voxelsList[i]->GetY(),voxelsList[i]->GetZ());

        hXY->GetZaxis()->SetRangeUser(0.1,1.05*Qmax1);
        hXZ->GetZaxis()->SetRangeUser(0.1,1.05*Qmax2);
        hYZ->GetZaxis()->SetRangeUser(0.1,1.05*Qmax3);

        if(NDFxy) fPrTracks[pT]->SetFitQuality(0, X2xy / NDFxy);
        else fPrTracks[pT]->SetFitQuality(0, 10000);

        if(NDFxz) fPrTracks[pT]->SetFitQuality(1, X2xz / NDFxz);
        else fPrTracks[pT]->SetFitQuality(1, 10000);

        if(NDFyz) fPrTracks[pT]->SetFitQuality(2, X2yz / NDFyz);
        else fPrTracks[pT]->SetFitQuality(2, 10000);

        fPrTracks[pT]->Set2DView(0,(TH2F*) hXY->Clone("hXY"));
        fPrTracks[pT]->Set2DView(1,(TH2F*) hXZ->Clone("hXZ"));
        fPrTracks[pT]->Set2DView(2,(TH2F*) hYZ->Clone("hYZ"));

        if(!p_Draw){
            delete hXY;
            delete hXZ;
            delete hYZ;
            delete hAll;
            continue;
        }

        cout << "Drawing ProtoTrack with " << voxelsList.size() << " voxels." << endl;

        TCanvas *cc = new TCanvas("cc","cc",0, 0, 1000,1000);
        cc->Divide(2,2);

        cc->cd(1);
        hXY->Draw("COLZ");
        cc->cd(2);
        hXZ->Draw("COLZ");
        cc->cd(3);
        hYZ->Draw("COLZ");

        RecoEvent::DrawProtoTracks(0);

        cc->cd(4);
        hAll->Draw("BOX2");

        cc->Update();
        cc->WaitPrimitive();

        delete cc;

        if(gROOT->FindObject("fData")){
            TNtuple* auxNT = (TNtuple*) gROOT->FindObject("fData");
            delete auxNT;
        }
        delete hXY;
        delete hXZ;
        delete hYZ;
        delete hAll;     
    }
        
}

//*****************************************************************************
void RecoEvent::BreakPoliTracks(){
//*****************************************************************************

    if(fVoxels.size() > 100) {cout << "Skipping shower-like event. Total # of Voxels: " << fVoxels.size() << endl; return;}
    cout << "Total # of Voxels: " << fVoxels.size() << endl;


    for (UInt_t pT = 0; pT<fPrTracks.size(); pT++){

        vector <Voxel*> voxelsList = fPrTracks[pT]->GetVoxels();
        
        if(voxelsList.size()<20) continue;

        gStyle->SetOptStat(0);

        if ( !(fPrTracks[pT]->GetFitQuality(1) >1 || fPrTracks[pT]->GetFitQuality(1) >1)) continue;

        cout << "Drawing ProtoTrack with " << voxelsList.size() << " voxels." << endl;
        cout << "QualXY: " << fPrTracks[pT]->GetFitQuality(0) << endl;
        cout << "QualXZ: " << fPrTracks[pT]->GetFitQuality(1) << endl;
        cout << "QualYZ: " << fPrTracks[pT]->GetFitQuality(2) << endl;

        fPrTracks[pT]->SearchVertex();

        TCanvas *cc = new TCanvas("cc","cc",0, 0, 1000,1000);
        cc->Divide(2,2);

        cc->cd(1);
        fPrTracks[pT]->Get2DView(0)->Draw("COLZ");
        cc->cd(2);
        fPrTracks[pT]->Get2DView(1)->Draw("COLZ");
        cc->cd(3);
        fPrTracks[pT]->Get2DView(2)->Draw("COLZ");

        RecoEvent::DrawProtoTracks(0);

        cc->Update();
        cc->WaitPrimitive();

        delete cc;

        if(gROOT->FindObject("fData")){
            TNtuple* auxNT = (TNtuple*) gROOT->FindObject("fData");
            delete auxNT;
        } 
    }      
}

void computeQuality(){
    return;
}

//*****************************************************************************
void RecoEvent::MergeCandidates(){
//*****************************************************************************

    if(fVoxels.size() > 100) {cout << "Skipping shower-like event. Total # of Voxels: " << fVoxels.size() << endl; return;}
    cout << "Size of candidates: " << fPrTracks.size() << endl;

    for (UInt_t pTi = 0; pTi<fPrTracks.size(); pTi++){
        for (UInt_t pTj = 0; pTj<fPrTracks.size(); pTj++){
            continue;
        }
    }

}

//*****************************************************************************
void RecoEvent::FitVoxels(ProtoTrack* PrTrack){
//*****************************************************************************

    // MODIFY!!! FitVoxels has to be a method of VoxelSet!
    // Problem :: PrTracks and Tracks must call FitVoxels through inheritance and store the FitPar as inner variables!
    // Think about segmentation... ¿Class RecoSegment between VoxelSet and Track?

    // Structure
    // Fill RecoEvent ... go to voxels
    // Clustering fill ProtoTracks
    // Clean XTalk
    // Break ProtoTracks in segments (could be smaller prototracks)
    // Fit segments stablish criteria bla bla -> Fill tracks 
    //

    if(PrTrack->GetVoxels().size()<2) return;

    TGraph2D * gr = new TGraph2D(); 

    for (UInt_t N=0; N<PrTrack->GetVoxels().size(); N++) {
       gr->SetPoint(N, PrTrack->GetVoxels()[N]->GetX(), PrTrack->GetVoxels()[N]->GetY(), PrTrack->GetVoxels()[N]->GetZ());
    }

    vector <Double_t> p = line3Dfit(gr);
    PrTrack->SetDir(p);

    if(p.size()){
        cout << p[0] << endl;
        cout << p[1] << endl;
        cout << p[2] << endl;
        cout << p[3] << endl;

        // draw the fitted line
        Int_t n = 1000;
        Double_t t0 = 0;
        Double_t dt = 48;

        TPolyLine3D *l = new TPolyLine3D(n);
        for (int i = 0; i <n;++i) {
           double t = t0+ dt*i/n;
           double x,y,z;
            x = p[0] + p[1]*t;
            y = p[2] + p[3]*t;
            z = t;
           l->SetPoint(i,z,y,x);
        }
        l->SetLineColor(kRed);

        for (UInt_t N=0; N<PrTrack->GetVoxels().size(); N++) {
            Double_t totDist = distToFit(PrTrack->GetVoxels()[N]->GetX(),PrTrack->GetVoxels()[N]->GetY(),PrTrack->GetVoxels()[N]->GetZ(),p);
            cout << "distToFit: " << totDist << endl;
            if(totDist > 0.8) PrTrack->GetVoxels()[N]->SetCharge(10000);
        }
        //PrTrack->RecoDraw();
        RecoEvent::ChargeDraw();
        TCanvas *auxCanvas = (TCanvas*) gROOT->FindObject("cQ"); 
        auxCanvas->cd();
        l->Draw("same");
        auxCanvas->Update();
        auxCanvas->WaitPrimitive();
    }
    PrTrack->RecoDraw("clean");
}

//*****************************************************************************
void RecoEvent::FitVoxels(){
//*****************************************************************************

    bool DRAWWW = kTRUE;

    if(fVoxels.size()<2) return;

    //RecoEvent::FindRecoXTalkMC();

    TGraph2D * auxGr = new TGraph2D(); 

    for (UInt_t N=0; N<fVoxels.size(); N++) {
        //if(fVoxels[N]->GetRecoId() == 1) continue;
        auxGr->SetPoint(auxGr->GetN(), fVoxels[N]->GetX(), fVoxels[N]->GetY(), fVoxels[N]->GetZ());
    }

    if(auxGr->GetN() < 3) return;

    vector <Double_t> p = line3Dfit(auxGr);

    if(p.size()){

        // draw the fitted line
        Int_t n = 1000;
        Double_t t0 = 0;
        Double_t dt = 48;

        TPolyLine3D *l = new TPolyLine3D(n);
        for (int i = 0; i <n;++i) {
           double t = t0+ dt*i/n;
           double x,y,z;
            x = p[0] + p[1]*t;
            y = p[2] + p[3]*t;
            z = t;
           l->SetPoint(i,z,y,x);
        }
        l->SetLineColor(kRed);

        for (UInt_t N=0; N<fVoxels.size(); N++) {
            Double_t totDist = distToFit(fVoxels[N]->GetX(),fVoxels[N]->GetY(),fVoxels[N]->GetZ(),p);
            //cout << "distToFit: " << totDist << endl;
            if(totDist > fGlobal->GetMinDistToFit()) {fVoxels[N]->SetRecoId(1); }//fVoxels[N]->SetCharge(10000);}
            else {fVoxels[N]->SetRecoId(0);}
        }

        Double_t Good = 0;
        Double_t Bad  = 0;
        Double_t LostCore  = 0;

        for(UInt_t i=0; i<fVoxels.size(); i++){
            for(UInt_t j=0; j<fTrueVoxels.size(); j++){
                if(fVoxels[i]->GetX() == fTrueVoxels[j]->GetX() && fVoxels[i]->GetY() == fTrueVoxels[j]->GetY() && fVoxels[i]->GetZ() == fTrueVoxels[j]->GetZ()){

                    if(fTrueVoxels[j]->GetTrueId() == 0) fGlobal->SetTotalCore(fGlobal->GetTotalCore()+1); 

                    if(fVoxels[i]->GetRecoId() != 0 && fTrueVoxels[j]->GetTrueId() != 0) {Good++; fGlobal->SetGood(fGlobal->GetGood()+1);}
                    else if (fVoxels[i]->GetRecoId() == 0 && fTrueVoxels[j]->GetTrueId() == 0) {Good++; fGlobal->SetGood(fGlobal->GetGood()+1);}
                    else {
                        //fVoxels[i]->SetCharge(10000);
                        Bad++; 
                        fGlobal->SetBad(fGlobal->GetBad()+1);
                        if(fTrueVoxels[j]->GetTrueId() == 0){
                            LostCore++;
                            fGlobal->SetLostCore(fGlobal->GetLostCore()+1);
                        }
                    }
                }
            }
        }

        RecoEvent::RemoveRecoXTalk();
        // cout << "GOOD: " << Good << endl;
        // cout << "BAD: "  << Bad << endl;
        // cout << "LostCore: "  << LostCore << endl;
        // cout << "RATIO: " << 100*Good/(Good+Bad) << " %" << endl << endl;

        if(DRAWWW){
            if(!fVoxels.size()) return
            RecoEvent::ChargeDraw();
            TCanvas *auxCanvas = (TCanvas*) gROOT->FindObject("cQ"); 
            auxCanvas->cd();
            l->Draw("same");
            auxCanvas->Update();
            auxCanvas->WaitPrimitive();
        }
    }
}
