#ifndef _Global_H_
#define _Global_H_

#include <TObject.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TLegend.h>

class Global : public TObject {
private:
  TH1F* fRecoQ[64];
  TH1F* fTrueQ;
  TH2F* fGhost[2];
  TH2F* fDist[3];
  TH1F* fXTalkId;
  TH1F* fPullZ[3];
  TH2F* f2DPullZ[3];

  Int_t fGood;
  Int_t fBad;
  Int_t fLostCore;

  Int_t fTotalCore;

  Int_t fPullZcut;
  Int_t fMinAveQcut;

  Int_t fMinHitTime;
  Int_t fMaxHitTime;

  Double_t fMinDistToFit;

  bool  fGlobalMC;
  bool  fDrawEvent;
  Int_t  fDrawingOpt;

  bool fRemoveXTalk;

public:
  Global(){
    Start();
  };
  virtual ~Global() {};

  void Clear(Option_t* /*option*/="")
  {
    TObject::Clear();
  }

  TH1F* GetRecoQ(Int_t p_N) {return fRecoQ[p_N];}
  TH1F* GetTrueQ() {return fTrueQ;}
  TH2F* GetGhost(Int_t p_N) {return fGhost[p_N];}
  TH2F* GetDist(Int_t p_N) {return fDist[p_N];}
  TH1F* GetXTalkId() {return fXTalkId;}
  TH1F* GetPullZ(Int_t p_N) {return fPullZ[p_N];}
  TH2F* Get2DPullZ(Int_t p_N) {return f2DPullZ[p_N];}

  Int_t GetGood() {return fGood;}
  Int_t GetBad()  {return fBad;}
  Int_t GetLostCore()  {return fLostCore;}
  Int_t GetTotalCore()  {return fTotalCore;}

  bool  GetRemoveXTalk() {return fRemoveXTalk;}
  bool  GetMC() {return fGlobalMC;}
  bool  GetDrawEvent() {return fDrawEvent;}
  Int_t GetDrawingOption() {return fDrawingOpt;}

  Int_t GetPullZcut() {return fPullZcut;}
  Int_t GetMinAveQcut() {return fMinAveQcut;}

  Int_t GetMinHitTime() {return fMinHitTime;}
  Int_t GetMaxHitTime() {return fMaxHitTime;}

  Double_t GetMinDistToFit() {return fMinDistToFit;}

  void SetMinDistToFit(Double_t p_D) {fMinDistToFit = p_D;}
  void SetRemoveXTalk(bool p_bool) {fRemoveXTalk = p_bool;}
  void SetMC(bool p_bool) {fGlobalMC = p_bool;}
  void SetDrawEvent(bool p_bool) {fDrawEvent = p_bool;}
  void SetDrawingOption(Int_t p_N) {fDrawingOpt = p_N;}

  void SetMinHitTime(Int_t p_fMinHitTime) {fMinHitTime = p_fMinHitTime;}
  void SetMaxHitTime(Int_t p_fMaxHitTime) {fMaxHitTime = p_fMaxHitTime;}

  void SetPullZcut(Int_t p_fPullZcut) {fPullZcut = p_fPullZcut;}
  void SetMinAveQcut(Int_t p_fMinAveQcut) {fMinAveQcut = p_fMinAveQcut;}

  void SetGood(Int_t p_fGood) {fGood = p_fGood;}
  void SetBad (Int_t p_fBad)  {fBad  = p_fBad;}
  void SetLostCore (Int_t p_fLostCore)  {fLostCore  = p_fLostCore;}
  void SetTotalCore(Int_t p_N)  {fTotalCore = p_N;}

  //methods
  void Start(){

      fRemoveXTalk = kFALSE;
      fGlobalMC    = kFALSE;
      fDrawEvent   = kFALSE;
      fGood        = 0;
      fBad         = 0;
      fLostCore    = 0;
      fTotalCore   = 0;
      fPullZcut    = 0;
      fMinAveQcut  = 0;

      Global::DefineRecoQ();
      Global::DefineTrueQ();
      Global::DefineGhost();
      Global::DefineDist();
      Global::DefineXTalkId();
      Global::DefinePullZ();
      Global::Define2DPullZ();
  }

  void DefineRecoQ(){
    for(Int_t i=0; i<64; i++){
        fRecoQ[i] = new TH1F(TString::Format("fRecoQ_%d", i),TString::Format("fRecoQ_%d", i),100,0,300); 
        fRecoQ[i]->SetLineWidth(5);
        fRecoQ[i]->SetLineColor(i+2);
        fRecoQ[i]->SetTitle("");
        fRecoQ[i]->GetXaxis()->SetTitle("#p.e");
    }
  }

  void DefineTrueQ(){
        fTrueQ = new TH1F("fTrueQ", "fTrueQ",100,0,300); 
        fTrueQ->SetLineWidth(5);
        fTrueQ->SetLineColor(1);
        fTrueQ->SetTitle("");
        fTrueQ->GetXaxis()->SetTitle("#p.e");
  }

  void DefineDist(){
      fDist[0] = new TH2F("fXY","fXY",24,0,24,8,0,8);
      fDist[1] = new TH2F("fXZ","fXZ",24,0,24,48,0,48);
      fDist[2] = new TH2F("fYZ","fYZ",8,0,8,48,0,48);
  }

  void DefineXTalkId(){
      fXTalkId = new TH1F("fXTalkId","fXTalkId",4,0,4);
  }

  void NormTrueQ(){
      fTrueQ->Scale(1/fTrueQ->Integral("width"));
  }

  void NormRecoQ(){
    for(Int_t i=0; i<64; i++){
      if(fRecoQ[i]->GetEntries()) fRecoQ[i]->Scale(1/fRecoQ[i]->Integral("width"));
    }
  }

  void NormPullZ(){
    for(Int_t i=0; i<3; i++){
      if(fPullZ[i]->GetEntries()) fPullZ[i]->Scale(1/fPullZ[i]->Integral("width"));
    }
  }

  void DefineGhost(){
    fGhost[0] = new TH2F("fReal","fReal",100,0,300,100,0,300);
    fGhost[1] = new TH2F("fGhost","fGhost",100,0,300,100,0,300);
  }

  void DefinePullZ(){
    fPullZ[0] = new TH1F("fPullZCore","fPullZCore",1000,-100,100);
    fPullZ[1] = new TH1F("fPullZXTalk","fPullZXTalk",1000,-100,100);
    fPullZ[2] = new TH1F("fPullZGhost","fPullZGhost",1000,-100,100);

    fPullZ[0]->SetLineWidth(3);
    fPullZ[0]->SetLineColor(kRed);
    fPullZ[1]->SetLineWidth(3);
    fPullZ[1]->SetLineColor(kBlue);
    fPullZ[2]->SetLineWidth(3);
    fPullZ[2]->SetLineColor(kBlack);
  }

  void Define2DPullZ(){
    f2DPullZ[0] = new TH2F("f2DPullZCore", "f2DPullZCore", 1000,-100,100,1000,0,200);
    f2DPullZ[1] = new TH2F("f2DPullZXTalk","f2DPullZXTalk",1000,-100,100,1000,0,200);
    f2DPullZ[2] = new TH2F("f2DPullZGhost","f2DPullZGhost",1000,-100,100,1000,0,200);
  }

  TLegend* GetRecoQLegend(vector <Int_t> p_nums){
    TLegend *leg = new TLegend(0.1, 0.905, 0.9, 0.98);
    leg->SetBorderSize(0);
    if(p_nums.size()<4) leg->SetNColumns(p_nums.size());
    else leg->SetNColumns(4);
    for(UInt_t i=0; i<p_nums.size(); i++){
      if(p_nums[i] == 0) leg->AddEntry(fRecoQ[0], "All", "el");
      if(p_nums[i] == 1) leg->AddEntry(fRecoQ[1], "M1: 3f", "el");
      if(p_nums[i] == 2) leg->AddEntry(fRecoQ[2], "M1: XY,XZ", "el");
      if(p_nums[i] == 3) leg->AddEntry(fRecoQ[3], "M1: XY,YZ", "el");
      if(p_nums[i] == 4) leg->AddEntry(fRecoQ[4], "M1: XZ,YZ", "el");
      if(p_nums[i] == 5) leg->AddEntry(fRecoQ[5], "M1: XY", "el");
      if(p_nums[i] == 6) leg->AddEntry(fRecoQ[6], "M1: XZ", "el");
      if(p_nums[i] == 7) leg->AddEntry(fRecoQ[7], "M1: XY", "el");
      if(p_nums[i] == 8) leg->AddEntry(fRecoQ[8], "M2: 3f", "el");
      if(p_nums[i] == 9) leg->AddEntry(fRecoQ[9], "M2: XY,XZ", "el");
      if(p_nums[i] == 10) leg->AddEntry(fRecoQ[10], "M2: XY,YZ", "el");
      if(p_nums[i] == 11) leg->AddEntry(fRecoQ[11], "M2: XZ,YZ", "el");
      if(p_nums[i] == 12) leg->AddEntry(fRecoQ[12], "M2: XY", "el");
      if(p_nums[i] == 13) leg->AddEntry(fRecoQ[13], "M2: XZ", "el");
      if(p_nums[i] == 14) leg->AddEntry(fRecoQ[14], "M2: XY", "el");
      if(p_nums[i] == 15) leg->AddEntry(fRecoQ[15], "M3: 3f", "el");
      if(p_nums[i] == 16) leg->AddEntry(fRecoQ[16], "M3: XY,XZ", "el");
      if(p_nums[i] == 17) leg->AddEntry(fRecoQ[17], "M3: XY,YZ", "el");
      if(p_nums[i] == 18) leg->AddEntry(fRecoQ[18], "M3: XZ,YZ", "el");
      if(p_nums[i] == 19) leg->AddEntry(fRecoQ[19], "M3: XY", "el");
      if(p_nums[i] == 20) leg->AddEntry(fRecoQ[20], "M3: XZ", "el");
      if(p_nums[i] == 21) leg->AddEntry(fRecoQ[21], "M3: XY", "el");
    }  
  return leg; 
  }

  ClassDef(Global,1);
};

#endif
