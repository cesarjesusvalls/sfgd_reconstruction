#ifndef _RECO_EVENT_H_
#define _RECO_EVENT_H_

#include "VoxelSet.hxx"
#include "Global.hxx"
#include <TH1F.h>
#include <iostream>

class RecoEvent: public VoxelSet{

private:
    Event*                  fEvent;
    TClonesArray*           fHits;
    vector <ProtoTrack*>    fPrTracks;
    Global*                 fGlobal;
    TClonesArray*           fTrueHits;
    vector<Voxel*>          fTrueVoxels;        //To store the true voxels (if MC)
    Int_t                   fNumTrueHits;
    Int_t                   fNumGhosts;
    bool                    fMC;

public:

    //constructors
    RecoEvent(){
        Reset();
    };


    // RecoEvent(Global* gInfo, Event* p_Event, Double_t p_Qcut=0, Int_t p_MaxMultiplicity=3, bool p_Draw=kFALSE, bool p_MC=kFALSE, bool p_RemoveXTalk=kTRUE, bool show3DXTalk=kFALSE, bool show3DGhost=kFALSE){
    //     //This constructor is the core of the 'sfgd_reconstruction' analysis package.
    //     //The following calls to different methods determine the flow of information within the program.

    //     Reset();
    //     SetMC(p_MC);                                        //Sets the variables on its initial values
    //     fGlobal = gInfo;                                    //The RecoEvent gets access to the Global class that acts as a common storage object for the whole analysis. 
    //     RecoEvent::SetEvent(p_Event);                       //It sets fEvents as the input Event and fHits as the hits associated to input Event.
    //     if(p_MC && p_RemoveXTalk){
    //         RecoEvent::RemoveXTalk();                       //Modifies fEvent excluding XTalk hits shortening fHits 
    //         fTrueHits = this->GetEvent()->GetHits();        //Sets fTrueHits at this stage, since we need to recover it after merging to extract true information.                                                   //On the other hand, we can not FindTrueVoxels since we do not find Voxels with HitsToVoxels
    //         fNumTrueHits = this->GetEvent()->GetNHits();    //Necessary to stablish the length of loops to run FindTrueVoxels.
    //     }
    //     if(p_MC && !p_RemoveXTalk){
    //         fTrueHits = this->GetEvent()->GetHits();
    //         fNumTrueHits = this->GetEvent()->GetNHits();
    //     }

    //     RecoEvent::MergeHits(p_Qcut);                       //Creates a new Event that contains hits that are formed from suming all hits sharing a fiber.
    //                                                         //The function again updates fEvent and fHits now containing merged information. Is important to do this
    //                                                         //before applying HitsToVoxels to reduce the combinatorial possibilities (specially for MC).
    //     RecoEvent::HitsToVoxels();                          //Constructs 3D objects from 2D objects.
    //     //RecoEvent::ApplyCuts();                           //Apply cuts to remove or tag crosstalk;    ....>   Wait for Dana inputs!

    //     RecoEvent::Reconstruct3DCharge(p_MaxMultiplicity, kTRUE);            //We reconstruct the 3D charge for each Voxel.

    //     if(p_MC){
    //         RecoEvent::FindTrueVoxels(p_Qcut);              //Finds the true information of the event (if MC). It uses fTrueHits, fNumTrueHits to find and fill
    //                                                         //fTrueVoxels and Fill histograms from Global. 
    //         RecoEvent::FindXTalk(show3DXTalk); 
    //         RecoEvent::FindGhosts(show3DGhost);                      //Uses true information to count the number of hits and fill information in Global
    //     }
    //     if(p_Draw) RecoEvent::DrawEvent(kFALSE);             //kTRUE to draw prototracks.
    // };

    RecoEvent(Global* gInfo, Event* p_Event){
        
        //This constructor to develop clustering, tracking and vertexing

        Reset();
        fGlobal = gInfo;                                    
        SetMC(fGlobal->GetMC());                                          
        RecoEvent::SetEvent(p_Event);                            
        // fTrueHits = this->GetEvent()->GetHits();   
        // fNumTrueHits = this->GetEvent()->GetNHits();

        if(fMC && fGlobal->GetRemoveXTalk()){
            // fTrueHits = this->GetEvent()->GetHits();
            // fNumTrueHits = this->GetEvent()->GetNHits();
            RecoEvent::RemoveXTalk(); 
        }

        // RecoEvent::MergeHits(0);
        // RecoEvent::HitsToVoxels();

        // if(fMC)RecoEvent::Reconstruct3DCharge(3, kTRUE);
        // if(fMC)RecoEvent::FindTrueVoxels(0);

        // if(fMC)RecoEvent::FindRecoXTalkMC(); 
        // else RecoEvent::FindRecoXTalk();

        // RecoEvent::RemoveRecoXTalk();
        
        // RecoEvent::FindProtoTracks(this->GetX(),this->GetY(),this->GetZ());

        // //RecoEvent::BreakPoliTracks();

        // //RecoEvent::MergeCandidates();

        // // RecoEvent::FillFinalTracks();

        // if(fGlobal->GetDrawEvent()) RecoEvent::DrawEvent(kTRUE);
    };

    RecoEvent(vector <Voxel*> p_fVoxels){
        Reset();        
        AddVoxels(p_fVoxels);
        RecoEvent::FindProtoTracks(this->GetX(),this->GetY(),this->GetZ());
    };

    //destructor
    virtual ~RecoEvent(){
        //Reset(); 
        for ( ProtoTrack* PrTrack : fPrTracks) delete PrTrack;
        for ( Voxel*      Vox :  fVoxels)      delete Vox;
        fEvent->Clear();
        std::cout << "destroying RecoEvent" << std::endl;
    };

    //functions to store data
    void SetEvent(Event* p_fEvent) {
        fEvent = p_fEvent;
        this->SetHits(fEvent->GetHits());
    }
    
    void SetTrueVoxels (vector<Voxel*> p_fTrueVoxels) {fTrueVoxels = p_fTrueVoxels; } 
    void SetHits(TClonesArray* p_fHits) { fHits = p_fHits; }
    void SetMC(bool p_fMC) {fMC = p_fMC;}

    //functions to retrieve data
    bool GetMC() { return fMC;}
    Event* GetEvent() { return fEvent; }
    vector<Voxel*>  GetTrueVoxels() { return fTrueVoxels; } 
    ProtoTrack* GetProtoTrack(Int_t p_N){ return fPrTracks[p_N]; }
    vector <ProtoTrack*> GetProtoTracks(){ return fPrTracks; }
    TClonesArray* GetHits() { return fHits; }

    //methods
    void TagxTalkFlag();
    void MergeHits(Double_t minQ);
    void HitsToVoxels();

    void FindTrueVoxels(Double_t minQ);

    void FitVoxels(ProtoTrack* PrTrack);
    void FitVoxels();
    
    void FindTrackCandidates(Int_t maxIdx, bool p_Draw);
    void BreakPoliTracks();
    void MergeCandidates();
    void FillFinalTracks();

    void RemoveRecoXTalk();
    void FindRecoXTalkMC();
    void FindRecoXTalk();
    void FindXTalk(bool p_show3D);
    void FillOcupancy();
    void ChargeDraw();
    void FindGhosts(bool p_show3D);
    void FillTrueQHist();
    void Reconstruct3DCharge(Int_t p_MaxMultiplicity, bool fillGlobal);

    void RemoveXTalk();
    void DrawEvent(bool p_DrawPrTracks);
    void DrawProtoTracks(Int_t p_minHits);

    void FindProtoTracks(vector <Int_t> p_X, vector <Int_t> p_Y, vector <Int_t> p_Z);  

    void Reset(Option_t* /*option*/="")
    {
        fVoxels.clear();  
        fPrTracks.clear();
        fNumGhosts = 0;
    } 

    ClassDef(RecoEvent,1);
};

#endif
