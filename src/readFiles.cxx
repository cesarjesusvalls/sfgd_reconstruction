#include "../utils/global_header.h"

//********************************************************************
vector <RecoEvent *> readTree(TString p_pathFile, Int_t p_maxEvents = 1000, bool p_Draw=kFALSE, bool p_Write=kFALSE){
//********************************************************************
    vector <RecoEvent*> eventsList;

    if(p_Write){
    cout << endl << endl << "*********************************************" << endl;
    cout << "Reading file: " << p_pathFile.Data() << endl;
    cout << "*********************************************" << endl << endl;
    }

    vector<vector <int>>  viewXY;
    vector<vector <int>>  viewXZ;
    vector<vector <int>>  viewYZ;
    vector <int> auxVector;

    TFile *FileInput=new TFile(p_pathFile.Data(),"update");

    TTree *EventTree = (TTree*)FileInput->Get("AllEvents");
    TClonesArray* Hits=0;

    Event *event = new Event();

    TBranch *EvBranch  = EventTree->GetBranch("Event");
    EvBranch->SetAddress(&event);

    int nEvents = EventTree->GetEntries();

    for (Int_t i=0;i<nEvents;i++) {
        EventTree->GetEvent(i);
        Hits = event->GetHits();
        Hits->Clear();
    }

    double prevCharge = -1;
    double evtCharge = -1;

    int selEvents = 0;


    ///  ----- APPLY SOME SELECTION ON EVENTS: -----

    for (int iev=0; iev<nEvents; iev++){

        if(iev%100 == 0) cout << "Reading event " << iev << " with " << event->GetNHits() << " hits." << endl << endl;

        if(selEvents >= p_maxEvents) break;

        EventTree->GetEntry(iev);
        vector <Hit*> hitsList;

        for (int ihit=0; ihit<event->GetNHits(); ihit++){

            Hit* hit = (Hit*)Hits->At(ihit);

            if(ihit ==0 && evtCharge == hit->GetCharge()) break; 
            if(ihit == 0) evtCharge = hit->GetCharge();

            if(prevCharge == hit->GetCharge()) continue;

            prevCharge = hit->GetCharge();

            hitsList.push_back(hit);

            auxVector.clear();

            if(hit->GetView() == 0) {
                auxVector.push_back(hit->GetX());
                auxVector.push_back(hit->GetY());
                auxVector.push_back(ihit);
                viewXY.push_back(auxVector);
            }

            auxVector.clear();

            if(hit->GetView() == 1) {
                auxVector.push_back(hit->GetX());
                auxVector.push_back(hit->GetZ());
                auxVector.push_back(ihit);
                viewXZ.push_back(auxVector);
            }

            auxVector.clear();

            if(hit->GetView() == 2) {
                auxVector.push_back(hit->GetY());
                auxVector.push_back(hit->GetZ());
                auxVector.push_back(ihit);
                viewYZ.push_back(auxVector);
            }
        }

        if(viewXY.size()+viewXZ.size()+viewYZ.size() == 0) continue;

        viewXY.clear();
        viewXZ.clear();
        viewYZ.clear();

        selEvents++;
        if(p_Write){
            cout << "Reading event " << iev << " with " << event->GetNHits() << " hits." << endl << endl;
        }
        ///  ----- RECONSTRUCT SELECTED EVENTS: ----- 
        RecoEvent* recoEvent = new RecoEvent(event, Hits, p_Draw, p_Write);
        eventsList.push_back(recoEvent);
    }

    return  eventsList;
}


//********************************************************************
vector <RecoEvent *> readTree(TString p_pathFile, bool p_Draw){
//********************************************************************

return readTree(p_pathFile,1000,p_Draw, kFALSE);

}
