
// //********************************************************************
// void readData(TString pathFile){
// //********************************************************************

//     cout << endl << endl << "*********************************************" << endl;
//     cout << "Reading file: " << pathFile.Data() << endl;
//     cout << "*********************************************" << endl << endl;

//     gStyle->SetOptStat(0); 

//     int numEvt, View, X,Y,Z;
//     double Charge , RE, FE;
//     int prevNumEvt = -1;
//     int NewEv = 1;

//     ifstream fData(pathFile.Data());

//     vector<vector <int>>  viewXY;
//     vector<vector <int>>  viewXZ;
//     vector<vector <int>>  viewYZ;
//     vector <int> auxVector;

//     while (!fData.eof()) {
//         fData >> numEvt >> View >> X >> Y >> Z >> Charge >> RE >> FE;


//         if (prevNumEvt != numEvt && (NewEv)){

//             prevNumEvt = numEvt;
//             NewEv = 0;
//         }

//         if (prevNumEvt == numEvt && !NewEv){

//             auxVector.clear();

//             if(View == 0) {
//                 auxVector.push_back(X);
//                 auxVector.push_back(Y);
//                 viewXY.push_back(auxVector);
//             }

//             if(View == 1) {
//                 auxVector.push_back(X);
//                 auxVector.push_back(Z);
//                 viewXZ.push_back(auxVector);
//             }

//             if(View == 2) {
//                 auxVector.push_back(Y);
//                 auxVector.push_back(Z);
//                 viewYZ.push_back(auxVector);
//             }

//         }

//         if(prevNumEvt != numEvt && !NewEv){
            
//             NewEv = 1;
//             cout << "Event: " << numEvt << endl;
//             //if(numEvt < 60+1) continue;

//             Reconstruct3D(viewXY,viewXZ,viewYZ);

//             viewXY.clear();
//             viewXZ.clear();
//             viewYZ.clear();
//         }
//     }
// }
