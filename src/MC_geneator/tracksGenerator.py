import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as m3d
# Generate some data that lies along a line

ax = m3d.Axes3D(plt.figure())

NoiseOn = False

lengthX = 192
lengthY = 192
lengthZ = 56
commonVertex = False
evts = 100

for g in range(evts):

    ax.clear()
    ax.set_xlim(0, lengthX)
    ax.set_ylim(0, lengthY)
    ax.set_zlim(0, lengthZ)

    fileout = open("/Users/cjesus/Documents/PhD/SFGD/sfgd_reconstruction/data/evt" + str(g) + ".txt", "w")

    for i in range(np.random.randint(10, 20)):
        #NumPoints = (np.random.randint(20, 300))
        NumPoints = 300
        if(i > 0):
            if np.random.uniform(0, 1) > 0.2:
                commonVertex = True
            else:
                commonVertex = False

        if commonVertex == False:
            x1 = np.random.uniform(0, lengthX)
            y1 = np.random.uniform(0, lengthY)
            z1 = np.random.uniform(0, lengthZ)

        x2 = np.random.uniform(x1 - lengthX, x1 + lengthX)
        y2 = np.random.uniform(y1 - lengthY, y1 + lengthY)
        z2 = np.random.uniform(z1 - lengthZ, z1 + lengthZ)

        minVal = 0
        maxVal = 1

        x = np.mgrid[x1:x2:1j * NumPoints]
        if x1 < x2:
            x -= x1
            len_x_ini = abs(x2 - x1)
            if np.random.uniform(0, 1) > 0.5:
                x = 1 * np.sin((x + np.random.uniform(minVal, maxVal) * abs(x2)) / (0.5 * abs(x2)))
            else:
                x = 1 * np.sin((x - np.random.uniform(minVal, maxVal) * abs(x2)) / (0.5 * abs(x2)))
            if np.max(x) - np.min(x) > 1:
                continue
            len_x_fin = abs(x[NumPoints - 1] - x[0])
            if np.argmax(x) != NumPoints - 1 and np.argmax(x) != 0:
                continue
            if np.argmin(x) != 0 and np.argmin(x) != NumPoints - 1:
                continue
            x *= len_x_ini / len_x_fin
            x -= x[0]
            x += x1
        else:
            x -= x1
            len_x_ini = abs(x2 - x1)
            if np.random.uniform(0, 1) > 0.5:
                x = 1 * np.sin((x + np.random.uniform(minVal, maxVal) * abs(x1)) / (0.5 * abs(x1)))
            else:
                x = 1 * np.sin((x - np.random.uniform(minVal, maxVal) * abs(x1)) / (0.5 * abs(x1)))
            len_x_fin = abs(x[NumPoints - 1] - x[0])
            if np.argmax(x) != NumPoints - 1 and np.argmax(x) != 0:
                continue
            if np.argmin(x) != 0 and np.argmin(x) != NumPoints - 1:
                continue
            if np.max(x) - np.min(x) > 1:
                continue
            x *= len_x_ini / len_x_fin
            x -= x[0]
            x += x1

        y = np.mgrid[y1:y2:1j * NumPoints]
        # if y1 < y2:
        #     y -= y1
        #     len_y_ini = abs(y2 - y1)
        #     # y = 1 * np.sin((y + np.random.uniform(minVal, maxVal) * abs(y2)) / (np.random.uniform(0, maxVal) * abs(y2) + abs(y2)))
        #     y = np.sin(y / abs(y2))
        #     len_y_fin = abs(y[NumPoints - 1] - y[0])
        #     y *= len_y_ini / len_y_fin
        #     y -= y[0]
        #     y += y1
        # else:
        #     y -= y1
        #     len_y_ini = abs(y2 - y1)
        #     # y = 1 * np.sin((y + np.random.uniform(minVal, maxVal) * abs(y1)) / (np.random.uniform(0, maxVal) * abs(y1) + abs(y1)))
        #     y = np.sin(y / abs(y1))
        #     len_y_fin = abs(y[NumPoints - 1] - y[0])
        #     y *= len_y_ini / len_y_fin
        #     y -= y[0]
        #     y += y1

        z = np.mgrid[z1:z2:1j * NumPoints]
        # z -= z1
        # len_z_ini = (z2 - z1)
        # z = np.random.uniform(0, 1) * np.sin((z + np.random.uniform(0, 0.5) * z2) / (z2))
        # len_z_fin = z[119] - z[0]
        # z *= len_z_ini / len_z_fin
        # z -= z[0]
        # z += z1

        data = np.concatenate((x[:, np.newaxis],
                               y[:, np.newaxis],
                               z[:, np.newaxis]),
                              axis=1)

        # Perturb with some Gaussian noise
        #data += np.random.normal(size=data.shape) * np.random.uniform(0.2, 1.4)

        toErase = []
        for idx, q in enumerate(data):
            if q[0] < 0 or q[0] > lengthX:
                toErase.append(idx)
            if q[1] < 0 or q[1] > lengthY:
                toErase.append(idx)
            if q[2] < 0 or q[2] > lengthZ:
                toErase.append(idx)

        data = np.delete(data, toErase, 0)
        data = np.round(data, 0)

        data = np.unique(data, axis=0)

        # Verify that everything looks right.
        for idx, q in enumerate(data):
            fileout.write("{:{}f} {:{}f} {:{}f} {:{}f}\n".format(q[0], 0.0, q[1], 0.0, q[2], 0.0, i, 0.0))
        ax.scatter3D(*data.T, marker='s')

    if NoiseOn == True:
        numNoise = (np.random.randint(0, 400))

        xRandom = np.random.uniform(0, lengthX, numNoise)
        yRandom = np.random.uniform(0, lengthY, numNoise)
        zRandom = np.random.uniform(0, lengthZ, numNoise)

        dataRandom = np.concatenate((xRandom[:, np.newaxis],
                                     yRandom[:, np.newaxis],
                                     zRandom[:, np.newaxis]),
                                    axis=1)

        ax.scatter3D(*dataRandom.T, color='b', marker='s')
        plt.savefig("/Users/cjesus/Documents/PhD/SFGD/sfgd_reconstruction/data/evt" + str(g) + ".pdf")
        for idx, q in enumerate(dataRandom):
            fileout.write("{:{}f} {:{}f} {:{}f} {:{}f}\n".format(q[0], 0.0, q[1], 0.0, q[2], 0.0, i, 0.0))
    else:
        plt.savefig("/Users/cjesus/Documents/PhD/SFGD/sfgd_reconstruction/data/evt" + str(g) + ".pdf")
